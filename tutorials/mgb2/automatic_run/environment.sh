auxdir=` pwd | sed 's/\(.*\)\/.*/\1/'` # extract pathname
#echo `dirname $(dirname $auxdir)`
#auxdir=`dirname $0`

#echo $auxdir
#cd $auxdir

####################################################################################
HOME_DIR=`dirname $(dirname $auxdir)`            # epiq  directory
# number of processors
NPROC=$(lscpu | grep 'per socket' | awk '{print $4}')
if [ "$?" -ne "0" ]; then
    NPROC=1
fi

# Loading user path
. ../../user_paths.sh
####################################################################################
# Default working parameters (NOT TO BE CHANGED)
####################################################################################
EXAMPLE_DIR="${HOME_DIR}/tutorials/${NAME}/results"


PARA_PREFIX="mpirun -np ${NPROC}"
PARA_POSTFIX=" -nk ${NPROC}  -nb 1 -nt 1 -nd 1"

BIN_EPIQ_DIR="${HOME_DIR}/bin"
BIN_EPIK_DIR="${HOME_DIR}/bin"

BIN_EPIQ_DIR="/home/babby/git/epiq-develop/bin"
BIN_EPIK_DIR="/home/babby/git/epiq-develop/bin"


AUXBIN_DIR="${HOME_DIR}/side-software"
PSEUDO_DIR="${HOME_DIR}/tutorials/${NAME}/pseudo"


SCF_DIR="$EXAMPLE_DIR/scf"
NSCF_DIR="$EXAMPLE_DIR/nscf"
WAN_DIR="$EXAMPLE_DIR/wannier"
PH_DIR="$EXAMPLE_DIR/ph"
ELPH_DIR="$EXAMPLE_DIR/elph"
EPIQ_DIR="$EXAMPLE_DIR/epiq"
EPIK_DIR="$EXAMPLE_DIR/epik"



DIR_LIST="  $BIN_EPIQ_DIR $EXAMPLE_DIR $AUXBIN_DIR"
#DIR_LIST="$BIN_DIR  $BIN_EPIQ_DIR $BIN_WANN_DIR $EXAMPLE_DIR $AUXBIN_DIR"



#---------------------------------------------------------------
# executable and pseudopotentials
#---------------------------------------------------------------
BIN_LIST= # "pw.x ph.x pw2wannier90.x "
BIN_EPIQ_LIST="epiq.x  " #generate_kgrid.x"
BIN_WANN_LIST= # "wannier90.x"


#---------------------
#  checking directories
#---------------------


$ECHO "  checking that needed directories and files exist...\c"

# check for directories
for DIR in $DIR_LIST; do
    if test ! -d $DIR ; then 
        $ECHO
        $ECHO "  WARN: $DIR has to be created"
        mkdir -p $DIR
    fi
done

# check for executables
for FILE in $BIN_LIST ; do
    if test ! -x $BIN_DIR/$FILE ; then
        $ECHO
        $ECHO "ERROR: $BIN_DIR/$FILE not existent or not executable"
        $ECHO "Aborting"
        exit 1
    fi
done
for FILE in $BIN_EPIQ_LIST ; do
    if test ! -x $BIN_EPIQ_DIR/$FILE ; then
        $ECHO
        $ECHO "ERROR: $BIN_EPIQ_DIR/$FILE not existent or not executable"
        $ECHO "Aborting"
        exit 1
    fi
done
for FILE in $BIN_EPIK_LIST ; do
    if test ! -x $BIN_EPIK_DIR/$FILE ; then
        $ECHO
        $ECHO "ERROR: $BIN_EPIK_DIR/$FILE not existent or not executable"
        $ECHO "Aborting"
        exit 1
    fi
done
for FILE in $BIN_WANN_LIST ; do
    if test ! -x $BIN_WANN_DIR/$FILE ; then
        $ECHO
        $ECHO "ERROR: $BIN_WANN_DIR/$FILE not existent or not executable"
        $ECHO "Aborting"
        exit 1
    fi
done
$ECHO " done"
$ECHO


#---------------------------------------------------------------
#   redirection
#---------------------------------------------------------------
if [[ "0" = "$DEBUG" ]];
then
    I=0
    REDIR=log${I}
    #while [[ "$(ls -A $EXAMPLE_DIR/${REDIR}_out.txt)" ]]
    while test  -f $EXAMPLE_DIR/${REDIR}_out.txt
    do
        I=$((I+1))
        REDIR=log${I}
    done
    #REDIR="$EXAMPLE_DIR/log_out.txt 2>$EXAMPLE_DIR/log_err.txt"
    REDIR1=$EXAMPLE_DIR/${REDIR}_out.txt
    REDIR2=$EXAMPLE_DIR/${REDIR}_err.txt
    $ECHO "Redirection" "${REDIR1}" "${REDIR2}" 

    if test  -f $EXAMPLE_DIR/${REDIR1} ; then
        mv --backup=numbered $REDIR1 ${REDIR1}_old
        mv --backup=numbered $REDIR2 ${REDIR2}_old
    fi

    function mute {
        case "$1" in
            "on") 
                exec 3>&1 1>>$REDIR1
                exec 4>&2 2>>$REDIR2


                ;;
            "off") 
                exec 1>&3 3>&-
                exec 2>&4 4>&-
                ;;
            *)
        esac
    }

    mute on
elif [[ "99" = "$DEBUG" ]]; then
OMPI_ALLOW_RUN_AS_ROOT=1  
OMPI_ALLOW_RUN_AS_ROOT_CONFIRM=1
PARA_PREFIX="mpirun --allow-run-as-root   -np ${NPROC}"
    function mute {
        sleep 1
    }
fi




function check_failure {
    if [ $1 -eq 0 ]; then
        $ECHO " done"
    else
        $ECHO " ----> FAIL!!"
        exit 1
    fi
}

function check_dir {
    if test ! -d $1 ; then 
        $ECHO
        $ECHO "  WARN: $1 has to be created"
        mkdir -p $1
    fi
    if  [ "$(ls -A $1)" ]; then 
        $ECHO
        $ECHO "  WARN: storing old results: $1"
        if test  -d $1/_ph0 ; then 
            rm -r $1/_ph0 $1/${NAME}.save
        fi
        if test  -d  $1/${NAME}.save ; then 
            rm -r  $1/${NAME}.save
        fi
        if test  -f  $1/${NAME}.wfc1 ; then 
            rm -f $1/${NAME}.wfc*
        fi
        if  [ !"$(ls -A $1)" ]; then 
            mkdir -p ${1}_old
        fi
        mv -v --backup=numbered ${1} ${1}_old
        mkdir -p  ${1}
    fi
}

function clean_and_backup {
    if test  -f $1 ; then 
        mv --backup=numbered $1  $1_old
    fi
}





$ECHO "$NAME:"
$ECHO 
$ECHO "Summary of physical properties of current SCF for $NAME:"
printf  "\tSmear\tPw_cut\tRho_cut\tKpoints\tNbands\tEnergy\tPress\t Efermi \n" 
printf  "\t$SMEAR_SCF \t $PW_CUT \t $RHO_CUT \t $KPOINTS_SCF_t \t $NBANDS_SCF \t$energy \t $press \t $Efermi\n" 
$ECHO "(Scf data in $SCF_DIR)"

#----------------------

$ECHO
$ECHO "starting in $EXAMPLE_DIR"
$ECHO
$ECHO "Running ph2EPIq calculation for $NAME."




# how to run executables
if [ "1" = "$DEBUG" ];
then
    EPIQ_COMMAND="gdb $BIN_EPIQ_DIR/epiq.x"
    EPIQ_COMMAND_1="gdb ${BIN_EPIQ_DIR}/epiq.x"
    EPIK_COMMAND="gdb $BIN_EPIK_DIR/epik.x"
    PW_COMMAND="gdb $BIN_DIR/pw.x"
    DYN_COMMAND="gdb $BIN_DIR/dynmat.x"
    PH_COMMAND="gdb -args $BIN_DIR/ph.x"
else

    PW_COMMAND="$PARA_PREFIX $BIN_DIR/pw.x $PARA_POSTFIX"
    PW_COMMAND_nk1="mpirun -np ${NPROC} $BIN_DIR/pw.x -nk 1 -nd 1 -nb 1 -nt 1"
    PW_COMMAND_1="mpirun -np 1 $BIN_DIR/pw.x -nk 1 -nd 1 -nb 1 -nt 1"

    DOS_COMMAND="$PARA_PREFIX $BIN_DIR/dos.x $PARA_POSTFIX"
    BANDS_COMMAND="$PARA_PREFIX $BIN_DIR/bands.x $PARA_POSTFIX"
    FS_COMMAND="$BIN_DIR/fs.x "


    PW2WAN_COMMAND="$PARA_PREFIX $BIN_DIR/pw2wannier90.x  -nk 1 -nd 1 -nb 1 -nt 1"
    WAN_COMMAND="$BIN_WANN_DIR/wannier90.x"
    POSTWAN_COMMAND="$PARA_PREFIX $BIN_WANN_DIR/postw90.x"

    WFCK2R_COMMAND="$PARA_PREFIX $BIN_DIR/wfck2r.x -nk 1 -nd 1 -nb 1 -nt 1"
    DYN_COMMAND="$PARA_PREFIX $BIN_DIR/dynmat.x $PARA_POSTFIX"

    PH_COMMAND="$PARA_PREFIX $BIN_DIR/ph.x $PARA_POSTFIX"
    PH_COMMAND_nk1="mpirun -np ${NPROC} $BIN_DIR/ph.x -nk 1 -nd 1 -nb 1 -nt 1"
    PH_COMMAND_1="mpirun -np 1 $BIN_DIR/ph.x -nk 1 -nd 1 -nb 1 -nt 1"

    Q2R_COMMAND="$PARA_PREFIX $BIN_DIR/q2r.x $PARA_POSTFIX"
    MATDYN_COMMAND="$PARA_PREFIX $BIN_DIR/matdyn.x $PARA_POSTFIX"
    MATDYN_COMMAND_1="mpirun -np 1  $BIN_DIR/matdyn.x  -nk 1 -nd 1 -nb 1 -nt 1"
    PLOTBAND_COMMAND="$BIN_DIR/plotband.x"

    EPIQ_KMESH="mpirun -np 1  $BIN_EPIQ_DIR/generate_kgrid.x"
    EPIQ_KMESH_TPIBA="$BIN_EPIQ_DIR/generate_kgrid_tpiba.x"

    EPIQ_COMMAND="$PARA_PREFIX $BIN_EPIQ_DIR/epiq.x"
    EPIQ_COMMAND_1="mpirun -np 1 ${BIN_EPIQ_DIR}/epiq.x"

    EPIK_COMMAND="$PARA_PREFIX $BIN_EPIK_DIR/epik.x"
    EPIK_COMMAND_1="mpirun -np 1 ${BIN_EPIK_DIR}/epik.x"
fi

