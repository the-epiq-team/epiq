 &control
    dump_gR=.false.,
    read_dumped_gr=.true.,
    prefix='GaAs',
    calculation='el_relaxation',
    &end
 &electrons
    theta=300.0,
    ngauss=0,
    sigma_min=0.0025,
    sigma_max=0.0025,
    nsigma=1,
    efermi=5.5,
    ef_from_input=.true.,
    thr_compute_k=0.1,
    wan_lt = 5
    wan_start=5
    wan_end=7
    holes=.false.
    &end
 &phonons
    forceq_1bz=.true.
    diel=.true.
    read_modes=.false.,
    Fourier_interp_dyn=.true.,
    acoustic_sum_rule=.true.,
    nq1=8,
    nq2=8,
    nq3=8,
  &end
k-points
crystal
1
 0.5 0.5 0.5 1
q-points
automatic
50 50 50 0 0 0


