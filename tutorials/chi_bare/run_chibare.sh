# check whether echo needs the -e option
if [ "`echo -e`" = "-e" ]; then
   echo='echo'
else
   echo='echo -e'
fi

#####
# loop over the momenta
#####
nkx=100
nkz=1
theta=20
ef="1.35" #"1.3878"
cat > QP << EOF
 0.0000000   0.0000000   0.0000000
 0.0100000  -0.0000000   0.0000000
 0.0200000  -0.0000000   0.0000000
 0.0300000  -0.0000000   0.0000000
 0.0400000  -0.0000000   0.0000000
 0.0500000  -0.0000000   0.0000000
 0.0600000  -0.0000000   0.0000000
 0.0700000  -0.0000000   0.0000000
 0.0800000  -0.0000000   0.0000000
 0.0900000  -0.0000000   0.0000000
 0.1000000  -0.0000000   0.0000000
 0.1100000  -0.0000000   0.0000000
 0.1200000  -0.0000000   0.0000000
 0.1300000   0.0000000   0.0000000
 0.1400000  -0.0000000   0.0000000
 0.1500000   0.0000000   0.0000000
 0.1600000  -0.0000000   0.0000000
 0.1700000   0.0000000   0.0000000
 0.1800000  -0.0000000   0.0000000
 0.1900000   0.0000000   0.0000000
 0.2000000  -0.0000000   0.0000000
 0.2100000   0.0000000   0.0000000
 0.2200000  -0.0000000   0.0000000
 0.2300000   0.0000000   0.0000000
 0.2400000  -0.0000000   0.0000000
 0.2500000   0.0000000   0.0000000
 0.2600000   0.0000000   0.0000000
 0.2700000   0.0000000   0.0000000
 0.2800000  -0.0000000   0.0000000
 0.2900000   0.0000000   0.0000000
 0.3000000   0.0000000   0.0000000
 0.3100000   0.0000000   0.0000000
 0.3200000  -0.0000000   0.0000000
 0.3300000  -0.0000000   0.0000000
 0.3400000   0.0000000   0.0000000
 0.3500000   0.0000000   0.0000000
 0.3600000  -0.0000000   0.0000000
 0.3700000  -0.0000000   0.0000000
 0.3800000   0.0000000   0.0000000
 0.3900000   0.0000000   0.0000000
 0.4000000  -0.0000000   0.0000000
 0.4100000  -0.0000000   0.0000000
 0.4200000   0.0000000   0.0000000
 0.4300000   0.0000000   0.0000000
 0.4400000  -0.0000000   0.0000000
 0.4500000  -0.0000000   0.0000000
 0.4600000   0.0000000   0.0000000
 0.4700000   0.0000000   0.0000000
 0.4800000  -0.0000000   0.0000000
 0.4900000  -0.0000000   0.0000000
 0.5000000   0.0000000   0.0000000
EOF

nq=`wc QP|awk '{print $1}'`

rm chipath.theta_${theta}.nk_${nkx}_${nkx}_${nkz}_ef_${ef}.d
rm nestingpath.nk_${nkx}_${nkx}_${nkz}_ef_${ef}.d
q=1
while [ $q -le ${nq} ]
do
qxmom=`awk 'NR=='$q' {print$1}' QP`
qymom=`awk 'NR=='$q' {print$2}' QP`
qzmom=`awk 'NR=='$q' {print$3}' QP`

cat > MoTe2.chi_q_${q}.in << EOF
 &input_chi
   prefix = 'monolayer',
   theta=${theta},
   sigma_min=0.02,
   sigma_max=0.02,
   nsigma=1,
   sigma_dos_min=0.1,
   sigma_dos_max=0.1,
   nsigma_dos=1,
   ngauss=0,
   xq_mom(1)=${qxmom},
   xq_mom(2)=${qymom},
   xq_mom(3)=${qzmom},
   lchimat=.false.,
   ef_from_input=.true.,
   efermi=${ef},
   E_min=0d0,
   E_max=1d0,
   npoints=200,
 &end
automatic 
${nkx} ${nkx} ${nkz} 0 0 0 
EOF

echo "  running the epsilon calculation for qpt" ${q}
mpirun -np 4 ../../bin/epsilon.x < MoTe2.chi_q_${q}.in  > MoTe2.chi_q_${q}.out_${nkx}_${nkx}_${nkz}.theta_${theta}_ef_${ef}
echo " done"

mv monolayer.chibare_nomat0 monolayer.chibare_nomat0.q_${q}.nk_${nkx}_${nkx}_${nkz}.theta_${theta}_ef_${ef}
mv monolayer.nesting0 monolayer.nesting0.q_${q}.nk_${nkx}_${nkx}_${nkz}.theta_${theta}_ef_${ef}

awk '{if(NR==4) print $2, $3}' monolayer.chibare_nomat0.q_${q}.nk_${nkx}_${nkx}_${nkz}.theta_${theta}_ef_${ef} >> chipath.theta_${theta}.nk_${nkx}_${nkx}_${nkz}_ef_${ef}.d
awk '{if(NR==2) print $2, $3}' monolayer.nesting0.q_${q}.nk_${nkx}_${nkx}_${nkz}.theta_${theta}_ef_${ef} >> nestingpath.nk_${nkx}_${nkx}_${nkz}_ef_${ef}.d

(( q++ ))
done
echo "--------------END--------------"

