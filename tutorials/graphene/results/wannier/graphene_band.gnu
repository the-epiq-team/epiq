set style data dots
set nokey
set xrange [0: 4.03087]
set yrange [-10.03938 : 10.87458]
set arrow from  1.70365, -10.03938 to  1.70365,  10.87458 nohead
set arrow from  2.55547, -10.03938 to  2.55547,  10.87458 nohead
set xtics ("G"  0.00000,"K"  1.70365,"M"  2.55547,"G"  4.03087)
 plot "graphene_band.dat"
