
             +---------------------------------------------------+
             |                                                   |
             |                   WANNIER90                       |
             |                                                   |
             +---------------------------------------------------+
             |                                                   |
             |        Welcome to the Maximally-Localized         |
             |        Generalized Wannier Functions code         |
             |            http://www.wannier.org                 |
             |                                                   |
             |                                                   |
             |  Wannier90 Developer Group:                       |
             |    Giovanni Pizzi    (EPFL)                       |
             |    Valerio Vitale    (Cambridge)                  |
             |    David Vanderbilt  (Rutgers University)         |
             |    Nicola Marzari    (EPFL)                       |
             |    Ivo Souza         (Universidad del Pais Vasco) |
             |    Arash A. Mostofi  (Imperial College London)    |
             |    Jonathan R. Yates (University of Oxford)       |
             |                                                   |
             |  For the full list of Wannier90 3.x authors,      |
             |  please check the code documentation and the      |
             |  README on the GitHub page of the code            |
             |                                                   |
             |                                                   |
             |  Please cite                                      |
             |                                                   |
             |  [ref] "Wannier90 as a community code:            |
             |        new features and applications",            |
             |        G. Pizzi et al., J. Phys. Cond. Matt. 32,  |
             |        165902 (2020).                             |
             |        http://doi.org/10.1088/1361-648X/ab51ff    |
             |                                                   |
             |  in any publications arising from the use of      |
             |  this code. For the method please cite            |
             |                                                   |
             |  [ref] "Maximally Localized Generalised Wannier   |
             |         Functions for Composite Energy Bands"     |
             |         N. Marzari and D. Vanderbilt              |
             |         Phys. Rev. B 56 12847 (1997)              |
             |                                                   |
             |  [ref] "Maximally Localized Wannier Functions     |
             |         for Entangled Energy Bands"               |
             |         I. Souza, N. Marzari and D. Vanderbilt    |
             |         Phys. Rev. B 65 035109 (2001)             |
             |                                                   |
             |                                                   |
             | Copyright (c) 1996-2020                           |
             |        The Wannier90 Developer Group and          |
             |        individual contributors                    |
             |                                                   |
             |      Release: 3.1.0        5th March    2020      |
             |                                                   |
             | This program is free software; you can            |
             | redistribute it and/or modify it under the terms  |
             | of the GNU General Public License as published by |
             | the Free Software Foundation; either version 2 of |
             | the License, or (at your option) any later version|
             |                                                   |
             | This program is distributed in the hope that it   |
             | will be useful, but WITHOUT ANY WARRANTY; without |
             | even the implied warranty of MERCHANTABILITY or   |
             | FITNESS FOR A PARTICULAR PURPOSE. See the GNU     |
             | General Public License for more details.          |
             |                                                   |
             | You should have received a copy of the GNU General|
             | Public License along with this program; if not,   |
             | write to the Free Software Foundation, Inc.,      |
             | 675 Mass Ave, Cambridge, MA 02139, USA.           |
             |                                                   |
             +---------------------------------------------------+
             |    Execution started on 20Sep2022 at 10:24:26     |
             +---------------------------------------------------+
 
 ******************************************************************************
 * -> Using CODATA 2006 constant values                                       *
 *    (http://physics.nist.gov/cuu/Constants/index.html)                      *
 * -> Using Bohr value from CODATA                                            *
 ******************************************************************************
 

 Running in serial (with parallel executable)

                                    ------
                                    SYSTEM
                                    ------

                              Lattice Vectors (Ang)
                    a_1     2.458718   0.000000   0.000000
                    a_2    -1.229359   2.129311   0.000000
                    a_3     0.000000   0.000000  14.998178

                   Unit Cell Volume:      78.52108  (Ang^3)

                        Reciprocal-Space Vectors (Ang^-1)
                    b_1     2.555472   1.475403  -0.000000
                    b_2     0.000000   2.950807   0.000000
                    b_3     0.000000  -0.000000   0.418930
  
 *----------------------------------------------------------------------------*
 |   Site       Fractional Coordinate          Cartesian Coordinate (Ang)     |
 +----------------------------------------------------------------------------+
 | C    1   0.00000   0.00000   0.50000   |    0.00000   0.00000   7.49909    |
 | C    2   0.33333   0.66667   0.50000   |   -0.00000   1.41954   7.49909    |
 *----------------------------------------------------------------------------*
                                ------------
                                K-POINT GRID
                                ------------
  
             Grid size =  6 x  6 x  1      Total points =   36
  
  
 *---------------------------------- MAIN ------------------------------------*
 |  Number of Wannier Functions               :                 2             |
 |  Number of Objective Wannier Functions     :                 2             |
 |  Number of input Bloch states              :                15             |
 |  Output verbosity (1=low, 5=high)          :                 1             |
 |  Timing Level (1=low, 5=high)              :                 1             |
 |  Optimisation (0=memory, 3=speed)          :                 3             |
 |  Length Unit                               :               Ang             |
 |  Post-processing setup (write *.nnkp)      :                 T             |
 |  Using Gamma-only branch of algorithms     :                 F             |
 *----------------------------------------------------------------------------*
 *------------------------------- WANNIERISE ---------------------------------*
 |  Total number of iterations                :              7000             |
 |  Number of CG steps before reset           :                 5             |
 |  Trial step length for line search         :             2.000             |
 |  Convergence tolerence                     :         0.100E-14             |
 |  Convergence window                        :                 3             |
 |  Iterations between writing output         :                10             |
 |  Iterations between backing up to disk     :               100             |
 |  Write r^2_nm to file                      :                 F             |
 |  Write xyz WF centres to file              :                 F             |
 |  Write on-site energies <0n|H|0n> to file  :                 F             |
 |  Use guiding centre to control phases      :                 T             |
 |  Use phases for initial projections        :                 F             |
 |  Iterations before starting guiding centres:                 0             |
 |  Iterations between using guiding centres  :                 1             |
 *----------------------------------------------------------------------------*
 *------------------------------- DISENTANGLE --------------------------------*
 |  Using band disentanglement                :                 T             |
 |  Total number of iterations                :              4000             |
 |  Mixing ratio                              :             0.500             |
 |  Convergence tolerence                     :         1.000E-10             |
 |  Convergence window                        :                 3             |
 *----------------------------------------------------------------------------*
 *-------------------------------- PLOTTING ----------------------------------*
 |  Plotting interpolated bandstructure       :                 T             |
 |   Number of K-path sections                :                 3             |
 |   Divisions along first K-path section     :               100             |
 |   Output format                            :           gnuplot             |
 |   Output mode                              :               s-k             |
 *----------------------------------------------------------------------------*
 |   K-space path sections:                                                   |
 |    From: G       0.000  0.000  0.000     To: K       0.667 -0.333  0.000   |
 |    From: K       0.667 -0.333  0.000     To: M       0.500  0.000  0.000   |
 |    From: M       0.500  0.000  0.000     To: G       0.000  0.000  0.000   |
 *----------------------------------------------------------------------------*
 Time to read parameters        0.006 (sec)

 *---------------------------------- K-MESH ----------------------------------*
 +----------------------------------------------------------------------------+
 |                    Distance to Nearest-Neighbour Shells                    |
 |                    ------------------------------------                    |
 |          Shell             Distance (Ang^-1)          Multiplicity         |
 |          -----             -----------------          ------------         |
 |             1                   0.418930                      2            |
 |             2                   0.491801                      6            |
 |             3                   0.646042                     12            |
 |             4                   0.837860                      2            |
 |             5                   0.851824                      6            |
 |             6                   0.949267                     12            |
 |             7                   0.971534                     12            |
 |             8                   0.983602                      6            |
 |             9                   1.069100                     12            |
 |            10                   1.194828                     12            |
 |            11                   1.256790                      2            |
 |            12                   1.292085                     12            |
 |            13                   1.301183                     12            |
 |            14                   1.349588                     12            |
 |            15                   1.366960                     24            |
 |            16                   1.475403                      6            |
 |            17                   1.518264                     12            |
 |            18                   1.533727                     12            |
 |            19                   1.547607                     24            |
 |            20                   1.595930                     12            |
 |            21                   1.675720                      2            |
 |            22                   1.696710                     12            |
 |            23                   1.703649                      6            |
 |            24                   1.746398                     12            |
 |            25                   1.754401                     12            |
 |            26                   1.773214                     12            |
 |            27                   1.809033                     24            |
 |            28                   1.822029                     24            |
 |            29                   1.879798                     12            |
 |            30                   1.898533                     12            |
 |            31                   1.938127                     12            |
 |            32                   1.943067                     12            |
 |            33                   1.961198                     24            |
 |            34                   1.967205                      6            |
 |            35                   2.011317                     12            |
 |            36                   2.094650                      2            |
 +----------------------------------------------------------------------------+
 | The b-vectors are chosen automatically                                     |
 | The following shells are used:   1,  2                                     |
 +----------------------------------------------------------------------------+
 |                        Shell   # Nearest-Neighbours                        |
 |                        -----   --------------------                        |
 |                          1               2                                 |
 |                          2               6                                 |
 +----------------------------------------------------------------------------+
 | Completeness relation is fully satisfied [Eq. (B1), PRB 56, 12847 (1997)]  |
 +----------------------------------------------------------------------------+
 |                  b_k Vectors (Ang^-1) and Weights (Ang^2)                  |
 |                  ----------------------------------------                  |
 |            No.         b_k(x)      b_k(y)      b_k(z)        w_b           |
 |            ---        --------------------------------     --------        |
 |             1         0.000000    0.000000    0.418930     2.848966        |
 |             2         0.000000    0.000000   -0.418930     2.848966        |
 |             3         0.000000    0.491801    0.000000     1.378161        |
 |             4         0.000000   -0.491801    0.000000     1.378161        |
 |             5         0.425912    0.245901    0.000000     1.378161        |
 |             6         0.425912   -0.245901    0.000000     1.378161        |
 |             7        -0.425912   -0.245901    0.000000     1.378161        |
 |             8        -0.425912    0.245901    0.000000     1.378161        |
 +----------------------------------------------------------------------------+
 |                           b_k Directions (Ang^-1)                          |
 |                           -----------------------                          |
 |            No.           x           y           z                         |
 |            ---        --------------------------------                     |
 |             1         0.000000    0.000000    0.418930                     |
 |             2         0.000000    0.491801    0.000000                     |
 |             3         0.425912    0.245901    0.000000                     |
 |             4         0.425912   -0.245901    0.000000                     |
 +----------------------------------------------------------------------------+
  
 Time to get kmesh              0.013 (sec)
 *============================================================================*
 |                              MEMORY ESTIMATE                               |
 |         Maximum RAM allocated during each phase of the calculation         |
 *============================================================================*
 |                        Disentanglement            1.31 Mb                  |
 |                            Wannierise:            0.10 Mb                  |
 |                          plot_wannier:            0.10 Mb                  |
 *----------------------------------------------------------------------------*
  
 Starting a new Wannier90 calculation ...

 Time to write kmesh            0.019 (sec)

 Exiting... graphene.nnkp written.
