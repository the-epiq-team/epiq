## Welcome to EPIq!

EPIq (Electron-Phonon wannier Interpolation over k and q-points) is an open-source software for the calculation of electron-phonon related properties.

## Learn EPIq 
Visit our website if you are interested in learning on how to use EPIq:

https://the-epiq-team.gitlab.io/epiq-site/

If you are interested in the theory underlying EPIq:

https://www.sciencedirect.com/science/article/pii/S0010465523002953

## Requirements:
A Fortran90 compiler, BLAS & LAPACK libraries.

## Installation

Run the configure file with:

./configure 

which should produce a compilable make.sys file. If necessary, further modify the make.sys file manually, indicating the Fortran90 compiler you want to use and the correct library folder (the same settings used to compile wannier90 should always work).

Then simply: 

make all

## Contributing
We are open to contributions by anybody who wants to develop their own project within EPIq. Just contact us via gitlab or email.

## Authors and acknowledgment
EPIq developers, visit https://the-epiq-team.gitlab.io/epiq-site/.

## License
 GNU General Public License (v3)
