find .  -name "*.save" -type d -exec rm -rf {} \;
find .  -name "*dwf*" -type f -exec rm -rf {} \;
find .   -name "*wfc*" -type f -exec rm -rf {} \;
find .   -name "*\.bar*" -type f -exec rm -rf {} \;
find .   -name "*\.mixd*" -type f -exec rm -rf {} \;
find .   -name "*\.recover*" -type f -exec rm -rf {} \;
find . -empty -type d -delete
find . -empty -type f -delete
