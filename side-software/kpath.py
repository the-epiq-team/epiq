#!/bin/python
import numpy as np
import json
import sys

if not 1 == len(sys.argv[1:]):
    raise RuntimeError("\n\nNeed json file defining the path ")
ff = sys.argv[1]

with open(ff) as f:
  data = json.load(f)

x_n = [ np.array(x) for x in data['x_n'] ]
Npt_n = data['Npt_n'] 
if Npt_n ==1:
    Npt_i = 1
else:
    Npt_i = int( (Npt_n -1)/ (len(x_n) -1) )
    Npt_n = (len(x_n) -1) * Npt_i

# print(f'K_POINTS crystal')
print(f'{Npt_n+1}')
count = 0
for i_x in range(len(x_n)-1):
    
    dx = (x_n[i_x+1] - x_n[i_x] )/ Npt_i
    for i in range(Npt_i):
        x_i = x_n[i_x] + dx*(i)
        count += 1
        print(f'{x_i[0]}    {x_i[1]}    {x_i[2]}    1')
        
x_i = x_n[i_x] + dx*(i+1)
print(f'{x_i[0]}    {x_i[1]}    {x_i[2]}    1')


data["high_symm_pts"] = [ i*Npt_i for i in range(len(x_n)) ]

with open(ff,'w') as f:
    json.dump(data,f)
