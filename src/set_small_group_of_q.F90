!    EPIq v 1.2, Electron-phonon interaction over k and q points
!    Authors : Matteo Calandra, Gianni Profeta, Francesco Mauri, Giulio Volpato,
!    Nathalie Vast, Jelena Sjakste, Francesco Macheda, Giovanni Marini, Guglielmo Marchese

!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.

!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.

!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.

! This module is adapted from Quantum ESPRESSO

!-----------------------------------------------------------------------
  SUBROUTINE set_small_group_of_q(xq, nsymq, invsymq, minus_q)
!-----------------------------------------------------------------------
!
!  This routine is a driver that sets the small group of q. It rotates
!  the matrices s so that the first nsymq elements are the small group
!  of q, and tells to the calling code if minus_q is true.
!  It deals also with the case modenum /= 0
!
  use w90_constants, only :dp
  use espresso_dynamical_matrix, only : at, bg
  use symm_base, only : nsym, s, time_reversal
  use symm_base, only : copy_sym, inverse_s, s_axis_to_cart

  IMPLICIT NONE

  REAL(DP), INTENT(INOUT) :: xq(3)
  INTEGER, INTENT(INOUT) :: nsymq
  LOGICAL, INTENT(INOUT) :: minus_q, invsymq

  REAL(DP), ALLOCATABLE :: rtau(:,:,:)

  LOGICAL :: sym(48)
  LOGICAL :: sym_(48)

  sym(1:nsym)=.true.
  call smallg_q (xq, at, bg, nsym, s, sym, minus_q)

  IF ( .not. time_reversal ) minus_q = .false.

  sym_(:)=sym(:)

  nsymq = copy_sym ( nsym, sym )

  call inverse_s ( )
  !
  ! check if inversion (I) is a symmetry. If so, there should be nsymq/2
  ! symmetries without inversion, followed by nsymq/2 with inversion
  ! Since identity is always s(:,:,1), inversion should be s(:,:,1+nsymq/2)
  !
  invsymq = ALL ( s(:,:,nsymq/2+1) == -s(:,:,1) )
  !
  !  Since the order of the s matrices is changed we need to recalculate:
  !
  call s_axis_to_cart ( )
  !
  RETURN
  END SUBROUTINE set_small_group_of_q
!
!-----------------------------------------------------------------------
subroutine smallg_q (xq, at, bg, nrot, s, sym, minus_q)
  !-----------------------------------------------------------------------
  !
  ! This routine selects, among the symmetry matrices of the point group
  ! of a crystal, the symmetry operations which leave q unchanged.
  ! Furthermore it checks if one of the above matrices send q --> -q+G.
  ! In this case minus_q is set true.
  !
  !  input-output variables
  !
  use w90_constants, only :dp
  implicit none

  real(DP), parameter :: accep = 1.e-5_dp

  real(DP), intent(in) :: bg (3, 3), at (3, 3), xq (3)
  ! input: the reciprocal lattice vectors
  ! input: the direct lattice vectors
  ! input: the q point of the crystal

  integer, intent(in) :: s (3, 3, 48), nrot
  ! input: the symmetry matrices
  ! input: number of symmetry operations
  logical, intent(inout) :: sym (48), minus_q
  ! input-output: .true. if symm. op. S q = q + G
  ! output: .true. if there is an op. sym.: S q = - q + G
  !
  !  local variables
  !

  real(DP) :: aq (3), raq (3), zero (3)
  ! q vector in crystal basis
  ! the rotated of the q vector
  ! the zero vector

  integer :: irot, ipol, jpol
  ! counter on symmetry op.
  ! counter on polarizations
  ! counter on polarizations

  logical :: eqvect
  ! logical function, check if two vectors are equal
  !
  ! return immediately (with minus_q=.true.) if xq=(0,0,0)
  !
  minus_q = .true.
  if ( (xq (1) == 0.d0) .and. (xq (2) == 0.d0) .and. (xq (3) == 0.d0) ) &
       return
  !
  !   Set to zero some variables
  !
  minus_q = .false.
  zero(:) = 0.d0
  !
  !   Transform xq to the crystal basis
  !
  aq = xq
!  call cryst_to_cart (1, aq, at, - 1)
  !
  !   Test all symmetries to see if this operation send Sq in q+G or in -q+G
  !
  do irot = 1, nrot
     if (.not.sym (irot) ) goto 100
     raq(:) = 0.d0
     do ipol = 1, 3
        do jpol = 1, 3
           raq(ipol) = raq(ipol) + DBLE( s(ipol,jpol,irot) ) * aq( jpol)
        enddo
     enddo
     sym (irot) = eqvect (raq, aq, zero, accep)
100  continue
  enddo
  return
end subroutine smallg_q

