!    EPIq v 1.2, Electron-phonon interaction over k and q points
!    Authors : Matteo Calandra, Gianni Profeta, Francesco Mauri, Giulio Volpato,
!    Nathalie Vast, Jelena Sjakste, Francesco Macheda, Giovanni Marini, Guglielmo Marchese

!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.

!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.

!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.

subroutine transform2realspace()

  use kq_points
  use io_var
  use computing_var
  use parallel_var
  use espresso_dynamical_matrix
  use diel_constants
  use input_param
  use w90_constants, only : ryd2ev
  use wannier_window
  use phonon_freq_grid, only :allocate_phonon_freq_grid
  use dielectric, only :read_diel
  use quter_standalone, only : quter, fftinterp_mat2, impose_asr2
  use utility, only : identify_kpq_general_grid
  use w90_comms, only: comms_barrier
  use control_flags, only :do_commutator


  ALLOCATE(kpt_latt(3,num_kpts)) 
  ALLOCATE(u_matrix(num_wann,num_wann,num_kpts))
  ALLOCATE(u_matrix_q(num_wann,num_wann,num_kpts,nq))
  ALLOCATE(u_matrix_opt(num_bands,num_wann,num_kpts))
  ALLOCATE(u_matrix_q_opt(num_bands,num_wann,num_kpts,nq))
  ALLOCATE(indexk(num_kpts))
  ALLOCATE(indexkpq(num_kpts,nq))


  !----------------------------------------------------
  !      Identify k+q among the k vectors of the k-mesh |
  !----------------------------------------------------

  !
  ! indexkpq is the identity so it should be eliminated everywhere
  !

  do n=1,num_kpts
    indexk(n)=n
  enddo


  do iqph=1,nq
    call identify_kpq_general_grid(num_kpts,kpt_all,indexkpq(1,iqph),xq(1,iqph))  
  enddo

  !-------------------------
  !   ...just a re-labelling
  !-------------------------

  do k=1,num_kpts
    u_matrix_opt(:,:,k)=u_opt(:,:,indexk(k))
    u_matrix(:,:,k)=u_mat(:,:,indexk(k))
    kpt_latt(:,k)=kpt_all(:,indexk(k)) 
  enddo

  do iqph=1,nq
    do k=1,num_kpts
      u_matrix_q_opt(:,:,k,iqph)=u_opt(:,:,indexkpq(k,iqph))
      u_matrix_q(:,:,k,iqph)=u_mat(:,:,indexkpq(k,iqph))     
    enddo
  enddo


  call setup_WS_cell()


  !
  !  q2r for the dynamical matrix
  !    force_c are the force constant matrices in real space.
  !       I was a bit lazy so they are not on the same grid as the
  !       Wannier, as the wigner seitz cell is chosen differently in q2R. 
  !       It should not matter.
  !
  if(Fourier_interp_dyn) then
    if(.not.use_alternative_dyn) then
      allocate(xq_tpiba_pw(3,nq))
      xq_tpiba_pw=xq
      call cryst_to_cart(nq,xq_tpiba_pw,bg_local,1)
    endif

    CALL quter(nq1, nq2, nq3, nat,tau,at_local,bg_local, phi_q, xq_tpiba_pw, nR, gridR, force_c)

    if(acoustic_sum_rule) call impose_asr2(nat,nR,gridR,force_c)
    deallocate(xq_tpiba_pw)
    deallocate(phi_q)
  endif

  !  $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
  !  $
  !  $       DISENTANGLEMENT 
  !  $
  !  $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

  call parallelize_index_array(nq,idx_q_pp)

  if(.not.read_dumped_gR) then
    allocate(eig_ham_wann(num_wann,num_kpts))
    if(do_commutator) then
      allocate(gop_comm(num_wann,num_wann,3,num_kpts))
    endif


    !Disentanglement operations are carried out here.
    !Warning, the commutator is also read inside here.
    !The reading of commutator should be changed and shifted
    !somewhere else.


    call disentangle_g_H_comm(nq, indexk, indexkpq, eig_ham_wann)

    deallocate(u_matrix_opt)
    deallocate(g_matrix)
    deallocate(u_opt)
    deallocate(ndimwin,lwindow,eigval)

  endif

  call from_k_to_r(nrpts, nrpts_q, irvec, irvec_q, ndegen, kpt_latt, stdout)


  if(verbosity.eq.'high'.and.ionode) then
    call dump_localized_matelem(nrpts, nrpts_q, irvec, irvec_q, real_lattice, &
      pe_id, nd_nmbr, stdout)
  endif


  if (.not.interpolate_k) then
    !if(.not.interpolate_k.and.calculation.ne.'convert_g_and_h'.and.  &
    !     calculation.ne.'convert_g_and_h_and_chi')then

    write(stdout,*) '---------------------------------'
    write(stdout,*) '           JOB ended             '
    write(stdout,*) '---------------------------------'
    call comms_barrier()
    calculation=' '
    return
  end if

end subroutine transform2realspace
