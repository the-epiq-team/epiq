!    EPIq v 1.2, Electron-phonon interaction over k and q points
!    Authors : Matteo Calandra, Gianni Profeta, Francesco Mauri, Giulio Volpato,
!    Nathalie Vast, Jelena Sjakste, Francesco Macheda, Giovanni Marini, Guglielmo Marchese

!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.

!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.

!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.


!This is the main EPIq driver. 
subroutine epiq( )
  use w90_constants, only : pi,twopi,bohr,dp,ryd2ev  ! some constants
  use utility                                        ! some useful functions
  use diel_constants                                 ! for the polar case
  use parallel_var                                   ! for parallel execution
  use io_var, only : stdout, io_time, error                 ! for output and timing
  use linewidth                                      ! linewidth module
  use espresso_dynamical_matrix                      ! dynamical matrices variables
  use relaxation                                     ! el. relaxation time module
  use double_resonant_raman                          ! double resonant Raman module
  use proj_on_path                                   ! project operators on k-path
  use wannier_window                                 ! Wannier related variables
  use kq_points                                      ! k and q point variables
  use cpu_timing                                     ! execution timing
  use input_param                                    ! input processing
  use symmetry_q                                     ! small group of q symmetry module
  use control_flags                                  ! some control parameters for the calculation 
  use migdal                                         ! some variables for M.E. calculation
  use header                                         ! header of EPIq
  use dump_matrix                                    ! dump matrix for step 1
  use phonon_freq_grid                               ! first step of frequency interpolation
  use phonon_freq                                    ! second step of frequency interpolation
  use computing_var                                  ! some extra variables
  IMPLICIT NONE
  include 'mpif.h'

  !external functions for smearing
  REAL(kind=dp)                 :: w0gauss,wgauss,dos_ef  
  INTEGER                       :: dyn_buffer_size


  !-----------------------------------------
  !write header
  !-----------------------------------------
  call header_q()


  !----------------------------------------------
  !initialize, read input file and wannier files
  !----------------------------------------------
  call init_and_read_input()

  !if (main_drive)&
    call read_aux_files()


  !----------------------------------------------
  !transform toward real space
  !----------------------------------------------
  
   if (main_drive)then
    call transform2realspace()
   end if



  !=====================================================
  !            Main loop of interpolation              |
  !====================================================  
  call epiq_header('Begin interpolation')


  !
  !  these variables should not be allocated if calculation
  !  is average lambda or a2F
  !
  allocate(g_int(num_wann,num_wann,nmodes))
  allocate(g_mu(num_wann,num_wann,nmodes))
  allocate(eig_int(num_wann),eig_int_q(num_wann))


  time_all(1)=io_time()

  SELECT CASE (TRIM(ADJUSTL(calculation)))

  !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
  CASE ('gmat')
  !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!


    call banner_dump_matrix()

    call allocate_dump_matrix() 

    call openfile_dump_matrix()

    call compute_dump_matrix()

    call closefile_dump_matrix()

    call deallocate_dump_matrix() 


    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
  CASE ('ph_linewidth') 
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

    call epiq_section('Phonon linewidth')

    if(main_drive)then
      call epiq_subsection('Running main-drive')

      call allocate_linewidth()

      call openfile_linewidth()

      call efermi_dos_info()

      call compute_linewidth()

      call closefile_linewidth()

      call deallocate_linewidth()

      call collect_file_linewidth()

    endif

    if(post_processing)then
      call epiq_subsection('Running post-proccessing')

      !call drr_postproc_spectrum()

    endif

    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
  CASE ('phonon_frequency_grid') 
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

    if(ionode)then
      call epiq_section('Phonon frequencies QE mesh ')

      !  Here I have no information on nk_int, only nk_int_ph exists. 
      !
      call efermi_info_freqgrid() 

      call compute_phonon_freq_grid()

      call deallocate_phonon_freq_grid()

    endif !(ionode)

    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
  CASE ('phonon_frequency') 
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!


    call epiq_section('Phonon frequencies')

    call efermi_info_pro(.true.) 

    call allocate_phonon_freq()

    call openfile_phonon_freq()

    call compute_phonon_freq()

    call closefile_phonon_freq()

    call deallocate_phonon_freq()

    call collect_file_phonon_freq()

    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
  CASE ('migdal_eliashberg') 
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
    
    call migdal_eliashberg_main()

    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
  CASE ('el_relaxation')
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

    time_all(1)=io_time()

    call epiq_section('Electron relaxation')

    call allocate_relaxation()

    call openfile_relaxation()

    call efermi_dos_info()

    call compute_relaxation()

    call closefile_relaxation()

    call deallocate_relaxation()

    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
  CASE ('resonant_raman')
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!


    call epiq_section('Resonant Raman')


    if(main_drive)then
      call epiq_subsection('Running main-drive')

      call allocate_resonant_raman()

      call efermi_info_pro(.false.) 

      call openfile_resonant_raman()

      call init_resonant_raman_param()

      call compute_resonant_raman()

      call closefile_resonant_raman()

      call collect_file_resonant_raman()

      call deallocate_resonant_raman()
    endif

    if(post_processing)then
      call epiq_subsection('Running post-proccessing')

      call drr_postproc_spectrum()

    endif

    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
  CASE ('proj_on_path')
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

    time_all(1)=io_time()

    call epiq_section('Project operator on path')

    call allocate_proj_on_path()

    call openfile_proj_on_path()

    call efermi_info_pro(.false.) 

    call project_on_path()

    call closefile_proj_on_path()

    call deallocate_proj_on_path()


  END SELECT

    time_all(2)=io_time()-time_all(1)
    call time_report_kq(calculation,stdout)

  write(stdout,*) '==================================='
  write(stdout,*) '         End interpolation       '
  write(stdout,*) '==================================='



  close(stdout)
  if(main_drive) then
    if(.not.read_dumped_gR) then
      deallocate(eig_ham_wann)
    endif

    if(interpolate_k) then
      deallocate(eig_int,eig_int_q)
      if(calculation.ne.'phonon_frequency_grid')  deallocate(xk_int)
      deallocate(g_int,g_mu)
    endif
    deallocate(u_matrix)
    deallocate(irvec,ndegen)
    deallocate(kpt_latt)

    deallocate(xq)
    deallocate(atm,amass,ityp,tau)  
  endif
end subroutine epiq




