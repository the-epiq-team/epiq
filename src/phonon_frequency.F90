!    EPIq v 1.2, Electron-phonon interaction over k and q points
!    Authors : Matteo Calandra, Gianni Profeta, Francesco Mauri, Giulio Volpato,
!    Nathalie Vast, Jelena Sjakste, Francesco Macheda, Giovanni Marini, Guglielmo Marchese

!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.

!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.

!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.

Module phonon_freq
  implicit none 
contains


  subroutine compute_phonon_freq()
    use io_var, only :  stdout
    use input_param 
    use control_flags, only :  compute_k
    use computing_var
    use dielectric 
    use noncollin_module, only: m_loc
    use parallel_var, only :  idx_q_pp,pe_id,nd_nmbr,&
      best_parall_k_VS_q,idx_k_pp,idx_k2_pp,ionode,&
      loop_percentage,nproc
    use symm_base, only : s, nsym, invs, time_reversal, irt, &
      set_sym_bl, find_sym, rotate_tau
    use symmetry_q, only : nsymq, minusq, invsymq, nqq, &
      isq, imq, sxq, rtau
    use control_flags, only :  adiabatic,non_adiabatic
    use cpu_timing
    use kq_points
    use io_var, only : io_time
    use w90_constants, only : pi,twopi,ryd2ev, bohr
    use espresso_dynamical_matrix
    use wannier_window
    use quter_standalone, only : quter, fftinterp_mat2, impose_asr2
    use utility, only : utility_frac_to_cart
    use w90_comms, only : comms_reduce,comms_array_split
    implicit none

    logical ::parallel_on_k
    integer :: ik
    integer, dimension(nproc) :: counts,displs


    COMPLEX(KIND=DP), ALLOCATABLE :: dynwork(:,:)
    complex(kind=dp) :: g_int_q(num_wann,num_wann,nmodes,nrpts)

    time_read_dynmat(2)=io_time()
    time_interpolate(2)=io_time()
    time_nonanaldyn(2)=io_time()
#ifdef TIMING
    time_fft_r2k_int(2)=io_time()
    time_get_Uint(2)=io_time()
    time_UgU(2)=io_time()
#endif

    !
    ! eig_int and eig_int_q are in eV
    ! g_int is in eV/bohr
    !

    call best_parall_k_VS_q(parallel_on_k,nq_int,idx_q_pp,nk_int,idx_k_pp,&
      nk_int_ph,idx_k2_pp)
    ! choosing the best parallelization
    if(parallel_on_k) write(stdout,'(5x,a)') 'Starting loop over q points:'
    !print*,pe_id,'-',idx_q_pp(1),idx_k_pp(1),idx_k2_pp(1)

    if(.not.parallel_on_k)then
      call comms_array_split(nq_int, counts, displs)
      nq_buffer=min(nq_buffer,counts(pe_id))
    endif

    !allocate(phi(3,3,nat,nat))
    do iq=1,size(idx_q_pp)
      iqph=idx_q_pp(iq)
      if (diel) then
        call diel_matr(xq_int(1,iqph),diel_term)
      end if

      call loop_percentage("Frequencies q loop:",iq,size(idx_q_pp),iloop_in=2)

      if(mod(iq-1,nq_buffer).eq.0) then
        write(stdout,'(5x,a,a)',advance='no') 'Starting reading dynmat pe=',nd_nmbr
        time_read_dynmat(1)=io_time()
        iqph_start_buf=iq-1
        call read_interp_dyn(iqph,nq_buffer,nq_int,nat,dyn_interp,xq_int,real_lattice,&
          celldm,idynunit) 
        time_read_dynmat(2)=time_read_dynmat(2)+io_time()-time_read_dynmat(1)
        write(stdout,*) '    ...done'
        !   write(stdout,*) 'finished reading dynmat pe=',nd_nmbr
        if(non_adiabatic) dyn_interp_NA=dyn_interp
      endif


      !print*,'dyn_interp', dyn_interp(:,:,1)

      nonanal_dyn_ph=CMPLX(0.d0,0.d0)

      !
      !  Now the nonanalytical term calculated on the ph mesh
      !
      time_interpolate(1)=io_time()
      g_int_q=0d0
      call interp_on_q(xq_int(1,iqph),irvec_q,ndegen_q, &
                num_wann,nmodes, nrpts,nrpts_q, g_int_q,g_op_r,nq, &
                eig_int_q)
      time_interpolate(2)=time_interpolate(2)+io_time()-time_interpolate(1)

      nsigma_ph=1
      dsigma_ph=0.d0
      !write(6,'(/)')
      DO ik=1,size(idx_k2_pp)
        if(ionode)&
          call loop_percentage("Frequencies raw k loop:",ik,size(idx_k2_pp))
        !DO k=1,nk_int_ph      
        k=idx_k2_pp(ik)
        k_int=xk_int_ph(:,k)
        !if(mod(k,int(nk_int_ph/10)).eq.0) print*,pe_id,'-',k,'/',nk_int_ph
        !if(mod(k,int(nk_int_ph/10)).eq.0) write(6,'(i3,a,i3,a)',advance='NO'),pe_id,'-', int(k/(nk_int_ph/10))*10,'%'

        time_interpolate(1)=io_time()

        call interp_on_k_after_q(k_int, irvec, xq_int(1,iqph),irvec_q,ndegen,ndegen_q, &
          num_wann,nmodes, ham_r ,nrpts,nrpts_q, g_int,g_int_q,g_op_r,num_kpts, nq, &
          eig_int,eig_int_q) 
        time_interpolate(2)=time_interpolate(2)+io_time()-time_interpolate(1)

        eig_int=eig_int/ryd2eV
        eig_int_q=eig_int_q/ryd2eV

        time_nonanaldyn(1)=io_time()



        call calculate_nonanal_dyn(sigma_ph,1,0.d0,ngauss_ph,num_wann,nmodes,nk_int_ph, &
          eig_int,eig_int_q,efermi_ph,g_int,nonanal_dyn_ph,wk_ph(k))

        !call calculate_nonanal_dyn(sigma_ph,nsigma_ph,dsigma_ph,ngauss_ph,num_wann,nmodes,nk_int_ph, &
        !    eig_int,eig_int_q,efermi_ph,g_int,nonanal_dyn_ph)


        !           call calculate_nonanal_dyn_1_smearing(sigma_ph,nsigma_ph,num_wann,nmodes,nk_int_ph, &
        !                eig_int,eig_int_q,efermi_ph,g_int,nonanal_dyn_ph)
        time_nonanaldyn(2)=time_nonanaldyn(2)+io_time()-time_nonanaldyn(1)

      enddo !loop on k points

      if(parallel_on_k)&
        call comms_reduce(nonanal_dyn_ph(1,1), nmodes*nmodes, 'SUM')

      !print*,'nonanal_dyn_ph', nonanal_dyn_ph(:,:)

      ! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ TEST $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      ! 
      ! write(stdout,*) 'Dynamical matrix read from file'
      ! 
      ! 
      ! 
      ! do i = 1, 3 * nat
      !  na = (i - 1) / 3 + 1
      !  icar = i - 3 * (na - 1)
      !    do j = 1, 3 * nat
      !       nb = (j - 1) / 3 + 1
      !       jcar = j - 3 * (nb - 1)
      !       phi(icar,jcar,na,nb)=dyn_interp(i,j,iqph-iqph_start_buf)
      !    enddo
      ! enddo
      ! 
      ! 
      !  call write_dyn_on_file(xq_s,phi,nat,stdout)
      !  stop
      !
      !write(stdout,*) 'sigma_ph=',sigma, ' ngauss_ph=',ngauss_ph,'  efermi_ph=',efermi_ph*ryd2eV
      !write(stdout,*) 'nonanal_dyn_ph'
      !
      ! Below I write the quantity to be subtracted
      !
      !
      !do i = 1, 3 * nat
      !   na = (i - 1) / 3 + 1
      !   icar = i - 3 * (na - 1)
      !   do j = 1, 3 * nat
      !      nb = (j - 1) / 3 + 1
      !      jcar = j - 3 * (nb - 1)
      !      phi(icar,jcar,na,nb)=nonanal_dyn_ph(i,j)
      !   enddo
      !enddo
      !
      !   write dyn.
      !
      !
      !
      !call write_dyn_on_file(xq_int(1,iqph),phi,nat,stdout)
      !
      !$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$4



      !
      !  I calculate the nonanalytical term calculated on the wannier interpolated dense mesh
      !

      nonanal_dyn_NA=CMPLX(0.d0,0.d0)
      nonanal_dyn_s   =CMPLX(0.d0,0.d0)
   !   time_interpolate(1)=io_time()
   !   call interp_on_q(xq_int(1,iqph),irvec_q,ndegen_q, &
   !             num_wann,nmodes, nrpts,nrpts_q, g_int_q,g_op_r,nq, &
   !             eig_int_q)
    !  time_interpolate(2)=time_interpolate(2)+io_time()-time_interpolate(1)

      DO ik=1,size(idx_k_pp)
        if(ionode)&
          call loop_percentage("Frequencies refined k loop:",ik,size(idx_k_pp))
        k = idx_k_pp(ik)
        k_int=xk_int(:,k)
        !if(mod(k,int(nk_int/10)).eq.0) print*,pe_id,'-',k,'/',nk_int
        !if(mod(k,int(nk_int/10)).eq.0) write(6,'(i3,a,i3,a)',advance='NO'),pe_id,'-', int(k/(nk_int/10))*10,'%'

        time_interpolate(1)=io_time()
        call interp_on_k_after_q(k_int, irvec, xq_int(1,iqph),irvec_q,ndegen,ndegen_q, &
          num_wann,nmodes, ham_r ,nrpts,nrpts_q, g_int,g_int_q,g_op_r,num_kpts, nq, &
          eig_int,eig_int_q) 
        time_interpolate(2)=time_interpolate(2)+io_time()-time_interpolate(1)

        eig_int=eig_int/ryd2eV
        eig_int_q=eig_int_q/ryd2eV

        time_nonanaldyn(1)=io_time()

        if(adiabatic) then
          call calculate_nonanal_dyn(sigma_min_ryd,nsigma,dsigma_ryd,ngauss,num_wann,nmodes,nk_int, &
            eig_int,eig_int_q,efermi_calc,g_int,nonanal_dyn_s,wk(k))
        endif

        if(non_adiabatic) then

          call calculate_NA_nonanal_dyn(sigma_min_ryd,nsigma,dsigma_ryd,ngauss,num_wann,nmodes,nk_int, &
            eig_int,eig_int_q,efermi_calc,g_int,broad_lst,size(broad_lst),freq_lst,size(freq_lst),nonanal_dyn_NA,defects,wk(k))
        endif

        time_nonanaldyn(2)=time_nonanaldyn(2)+io_time()-time_nonanaldyn(1)


      enddo !loop on k points


      !print*,'nonanal',nonanal_dyn_NA(:,:,1,1)
      !print*,'nonanal', nonanal_dyn_s(:,:,1)


      !$$$$$$$$$$$$$$$$$$$$$$$$$ TEST $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
      !
      !
      ! Below I write the quantity to be added
      !
      !
      !        do i = 1, 3 * nat
      !           na = (i - 1) / 3 + 1
      !           icar = i - 3 * (na - 1)
      !           do j = 1, 3 * nat
      !              nb = (j - 1) / 3 + 1
      !              jcar = j - 3 * (nb - 1)
      !             phi(icar,jcar,na,nb)=nonanal_dyn_s(i,j,1)
      !             phi(icar,jcar,na,nb)=nonanal_dyn_NA(i,j,1,1)
      !           enddo
      !        enddo
      !

      !
      ! Below I write the difference
      !
      !
      !Write(stdout,*) 'And this is the difference'
      !
      !do i = 1, 3 * nat
      !   na = (i - 1) / 3 + 1
      !   icar = i - 3 * (na - 1)
      !   do j = 1, 3 * nat
      !      nb = (j - 1) / 3 + 1
      !      jcar = j - 3 * (nb - 1)
      !      phi(icar,jcar,na,nb)=nonanal_dyn_s(i,j,1)-nonanal_dyn_ph(i,j)
      !   enddo
      !enddo
      !call write_dyn_on_file(xq_int,phi,nat,stdout)
      !$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


      if(parallel_on_k)then
        if(adiabatic) then
          call comms_reduce(nonanal_dyn_s(1,1,1), nmodes*nmodes*size(broad_lst), 'SUM')
        end if
        if(non_adiabatic) then
          call comms_reduce(nonanal_dyn_NA(1,1,1,1), nmodes*nmodes*nsigma*size(broad_lst), 'SUM')
        endif
      end if


      ! only one pool

      if( (ionode.and.parallel_on_k) .or. (.not.parallel_on_k)   )then

        !
        ! Here I calculate the small group of q and the star of q.      
        !

        at(:,:)=at_local(:,:)
        bg(:,:)=bg_local(:,:)

        CALL set_sym_bl ( )
        CALL find_sym ( nat, tau, ityp, .not.time_reversal, m_loc)
        CALL set_small_group_of_q(xq_int(:,iqph), nsymq, invsymq, minusq)
        CALL star_q(xq_int(:,iqph), at, bg, nqq, sxq(:,:), isq(:), imq, .true.)
        CALL rotate_tau ( nat, tau, rtau)

        ALLOCATE(dynwork(nmodes,nmodes))
        !allocate(phi_q(3,3,nat,nat,48))

        if(adiabatic) then
          !
          ! here do a loop on sigma and define a new non anal dyn.
          !

          call utility_frac_to_cart(xq_int(:,iqph),xq_s,recip_lattice)
          xq_s=xq_s*(celldm(1)*bohr)/twopi

          do isigma=1,nsigma
            if(ionode)&
              call loop_percentage("Writring adiabatic frequencies:",isigma,nsigma)


            dynwork(:,:)=dyn_interp(:,:,iq-iqph_start_buf)-nonanal_dyn_ph(:,:)

            dynwork(:,:)=dynwork(:,:)+nonanal_dyn_s(:,:,isigma)

            !
            ! Symmetrize the dynamical matrix wrt the small group of q
            !

            !call q2qstar_ph_nowrite (dynwork, at_local, bg_local, nat, nsym, &
            !  s, invs, irt, rtau, nqq, sxq, isq, imq, phi_q)

            !
            ! Write the dynamical matrix.
            !



            ! Compact the first phi_q

            do i = 1, 3 * nat
              na = (i - 1) / 3 + 1
              icar = i - 3 * (na - 1)
              do j = 1, 3 * nat
                nb = (j - 1) / 3 + 1
                jcar = j - 3 * (nb - 1)
                 phi(icar,jcar,na,nb)=dynwork(i,j)
                !dynwork (i,j) = phi_q (icar,jcar,na,nb,1)
              enddo
            enddo
            if(write_dyn) call write_dyn_on_file(xq_s,phi,nat,iounit_sigma(isigma))

            !
            ! Diagonalize the dynamical matrix (Adiabatic case)
            !

            if(write_dyn) then
              call dyndia (xq_s,nmodes, nat, ntyp, ityp, amass, iounit_sigma(isigma), dynwork,&
                w_interp(1,1))
            else
              call dyndia (xq_s,nmodes, nat, ntyp, ityp, amass, stdout, dynwork,&
                w_interp(1,1))
            endif

            !
            ! ... write phonon modes.
            !

            if(write_modes) then
              call writemodes (nat,nat,xq_s,w_interp(1,1),dynwork,iounit3_sigma(isigma))
            endif

            !
            ! and write phonon frequencies.
            !



            write(iounit2_sigma(isigma),*)
            write(iounit2_sigma(isigma),'(f15.6)') (xq_s(i),i=1,3)
            write(iounit2_sigma(isigma),*)


            do nu = 1, nmodes
              if(w_interp(nu,1) < 0.d0) then
                w_interp (nu,1)= -sqrt(abs(w_interp (nu,1)))
              else
                w_interp (nu,1)= sqrt(abs(w_interp (nu,1)))
              endif

              WRITE( iounit2_sigma(isigma), '(1i4,4f15.7)') nu, w_interp (nu,1)  * ryd2ev * 1000.0, &
                w_interp (nu,1)  * rydthz, w_interp (nu,1) * ryd2cm1
            enddo

          enddo

        endif


        if(non_adiabatic) then

          call utility_frac_to_cart(xq_int(:,iqph),xq_s,recip_lattice)
          xq_s=xq_s*(celldm(1)*bohr)/twopi


          do isigma=1,nsigma
            if(ionode)&
              call loop_percentage("Writring non-adiabatic frequencies:",isigma,nsigma)
            do isigma_na=1,size(broad_lst)

              dynwork(:,:)=dyn_interp_NA(:,:,iq-iqph_start_buf)-nonanal_dyn_ph(:,:)

              dynwork(:,:)=dynwork(:,:)+nonanal_dyn_NA(:,:,isigma,isigma_na)

              !
              ! Symmetrize the dynamical matrix wrt the small group of q
              !

             ! call q2qstar_ph_nowrite (dynwork, at_local, bg_local, nat, nsym, &
             !   s, invs, irt, rtau, nqq, sxq, isq, imq, phi_q)              

              !
              ! Write the dynamical matrix
              !

            !  if (write_dyn) call write_dyn_on_file (xq_s,phi, &
            !    nat,i_NA_dyn_unit_sigma(isigma,isigma_na))


              ! Compact the first phi_q

              do i = 1, 3 * nat
                na = (i - 1) / 3 + 1
                icar = i - 3 * (na - 1)
                do j = 1, 3 * nat
                  nb = (j - 1) / 3 + 1
                  jcar = j - 3 * (nb - 1)
                   phi(icar,jcar,na,nb)=dynwork(i,j)
                  !dynwork (i,j) = phi_q (icar,jcar,na,nb,1)
                enddo
              enddo

                 if(write_dyn) call write_dyn_on_file(xq_s,phi,nat,i_NA_dyn_unit_sigma(isigma,isigma_na))

              !
              ! Diagonalize the dynamical matrix (non adiabatic case)
              !

              if(write_dyn) then
                call dyndia (xq_s,nmodes, nat, ntyp, ityp, amass, i_NA_dyn_unit_sigma(isigma,isigma_na), &
                  dynwork,w_interp(1,1))
              else
                call dyndia (xq_s,nmodes, nat, ntyp, ityp, amass, stdout, &
                  dynwork,w_interp(1,1))

              endif

              !
              ! and write phonon frequencies.(Non Adiabatic case)
              !

              write(i_NA_unit_sigma(isigma,isigma_na),*)
              write(i_NA_unit_sigma(isigma,isigma_na),'(f15.6)') (xq_s(i),i=1,3)
              write(i_NA_unit_sigma(isigma,isigma_na),*)

              do nu = 1, nmodes
                if(w_interp(nu,1) < 0.d0) then
                  w_interp (nu,1)= -sqrt(abs(w_interp (nu,1)))
                else
                  w_interp (nu,1)= sqrt(abs(w_interp (nu,1)))
                endif

                WRITE( i_NA_unit_sigma(isigma,isigma_na), '(1i4,4f15.7)') nu, &
                  w_interp (nu,1)  * ryd2ev * 1000.0, &
                  w_interp (nu,1)  * rydthz, w_interp (nu,1) * ryd2cm1
              enddo

              !
              ! ... write phonon modes.
              !

              if(write_modes) then
                call writemodes (nat,nat,xq_s,w_interp(1,1),dynwork,i_NA_modes_unit_sigma(isigma,isigma_na))
              endif
            enddo
          enddo

        endif

        !deallocate(phi_q)
        DEALLOCATE(dynwork)

      endif


      close(idynunit)
    enddo



  end subroutine compute_phonon_freq




  subroutine allocate_phonon_freq()

    use input_param, only : nsigma,read_modes,Fourier_interp_dyn,broad_lst
    use computing_var
    use espresso_dynamical_matrix
    use wannier_window, only :  num_wann
    use kq_points
    use io_var, only: stdout
    implicit none 

    !external functions
    INTEGER                       :: dyn_buffer_size

    !
    ! I determine the buffer size for reading the dynamical matrices.
    !

    nq_buffer=dyn_buffer_size(nq_int,nmodes,memory_dyn)
    write(stdout,'(5x,a,i5)') 'Buffer size for dynamical matrix is nq_buffer=',nq_buffer

    allocate(dyn_interp(nmodes,nmodes,nq_buffer))
    allocate(dyn_interp_NA(nmodes,nmodes,nq_buffer))
    allocate(nonanal_dyn_ph(nmodes,nmodes))
    allocate(nonanal_dyn_s(nmodes,nmodes,nsigma))
    allocate(nonanal_dyn_NA(nmodes,nmodes,nsigma,size(broad_lst)))
    allocate(dynstore(nmodes,nmodes))
    ALLOCATE ( phi(3,3,nat,nat) )
    ALLOCATE( w_interp(3*nat,1))

  end  subroutine allocate_phonon_freq

  subroutine deallocate_phonon_freq()
    use espresso_dynamical_matrix
    use computing_var
    use control_flags, only :  adiabatic,non_adiabatic
    use input_param, only : read_modes,Fourier_interp_dyn
    use io_var, only: stdout
    use parallel_var, only: ionode
    implicit none 

    !if(ionode) then
    if(adiabatic) then
      deallocate(nonanal_dyn_s,nonanal_dyn_ph)
      deallocate(iounit_sigma)
      deallocate(iounit2_sigma)
      deallocate(iounit3_sigma)
    endif

    if(non_adiabatic) then
      deallocate(nonanal_dyn_NA)
      deallocate(i_NA_dyn_unit_sigma,i_NA_unit_sigma)
    endif


    deallocate(dynstore)
    deallocate(w_interp)
    deallocate(phi)
    !endif

  end  subroutine deallocate_phonon_freq



  subroutine openfile_phonon_freq()
    use w90_constants, only :dp,ryd2ev
    use computing_var, only:isigma,i_NA_unit_sigma,i_NA_modes_unit_sigma,&
      i_NA_dyn_unit_sigma,iounit_sigma,isigma_na,iounit2_sigma,&
      iounit3_sigma,idynunit,postfix,postfix2,degauss2,g_op_r,efermi_ph, &
      efermi_calc
    use input_param, only : nsigma,write_modes,write_dyn,&
      broad_lst,freq_lst,interp_dyn_file,sigma_min,dsigma,degauss,&
      fil_dyn_ext,sigma_ph
    use io_var, only : io_file_unit
    use parallel_var, only : nd_nmbr,ionode
    use control_flags, only :  adiabatic,non_adiabatic

    implicit none 


    ! I convert everything to rydberg:
    sigma_ph=sigma_ph/ryd2ev
    efermi_ph=efermi_ph/ryd2ev
    g_op_r=g_op_r/ryd2ev
    efermi_calc=efermi_calc/ryd2ev
    broad_lst=broad_lst/ryd2ev
    freq_lst=freq_lst/ryd2ev
    !
    ! open file with interpolated dynamical matrices (reading)
    !

    idynunit=io_file_unit()
    open(unit=idynunit,file=trim(adjustl(interp_dyn_file)),&
      status='unknown',form='formatted')
    rewind(idynunit)

    !
    ! open files for output
    !


    !if(ionode) then

    if(adiabatic) then
      allocate(iounit_sigma(nsigma),iounit2_sigma(nsigma),iounit3_sigma(nsigma))
      do isigma=1,nsigma
        degauss=sigma_min+(isigma-1)*dsigma
        degauss=degauss/ryd2eV
        write(postfix(1:12),'(1f12.6)') degauss
        postfix=trim(adjustl(postfix))
        if(write_dyn) then
          iounit_sigma(isigma)=io_file_unit()
          open(unit=iounit_sigma(isigma),&
            file=trim(adjustl(fil_dyn_ext))//'.dyn.pe_'//nd_nmbr//'sigma'//postfix(1:6),&
            !file=trim(adjustl(fil_dyn_ext))//'.dyn.sigma'//postfix(1:6),&
          status='unknown',form='formatted')
          rewind(iounit_sigma(isigma))
        endif 

        iounit2_sigma(isigma)=io_file_unit()
        open(unit=iounit2_sigma(isigma),&
          file=trim(adjustl(fil_dyn_ext))//'.freq.pe_'//nd_nmbr//'sigma'//postfix(1:6),&
          !file=trim(adjustl(fil_dyn_ext))//'.freq.sigma'//postfix(1:6),&
        status='unknown',form='formatted')
        rewind(iounit2_sigma(isigma))

        if(write_modes) then
          iounit3_sigma(isigma)=io_file_unit()
          open(unit=iounit3_sigma(isigma),&
            file=trim(adjustl(fil_dyn_ext))//'.modes.pe_'//nd_nmbr//'sigma'//postfix(1:6),&
            !file=trim(adjustl(fil_dyn_ext))//'.modes.sigma'//postfix(1:6),&
          status='unknown',form='formatted')
          rewind(iounit3_sigma(isigma))
        endif
      enddo
    endif

    if(non_adiabatic) then

      allocate(i_NA_dyn_unit_sigma(nsigma,size(broad_lst)),i_NA_unit_sigma(nsigma,size(broad_lst)))
      allocate(i_NA_modes_unit_sigma(nsigma,size(broad_lst)))
      do isigma=1,nsigma
        degauss=sigma_min+(isigma-1)*dsigma
        degauss=degauss/ryd2eV
        write(postfix(1:12),'(1f12.6)') degauss
        postfix=trim(adjustl(postfix))
        do isigma_na=1,size(broad_lst)
          degauss2=broad_lst(isigma_na)
          write(postfix2(1:12),'(1f12.6)') degauss2
          postfix2=trim(adjustl(postfix2))

          if(write_dyn) then
            i_NA_dyn_unit_sigma(isigma,isigma_na)=io_file_unit()
            open(unit=i_Na_dyn_unit_sigma(isigma,isigma_na),&
              file=trim(adjustl(fil_dyn_ext))//'.NA_dyn.pe_'//nd_nmbr//'sig'//postfix(1:6)//'sigNA'//postfix2(1:6),&
              !file=trim(adjustl(fil_dyn_ext))//'.NA_dyn.sig'//postfix(1:6)//'sigNA'//postfix2(1:6),&
            status='unknown',form='formatted')
            rewind(i_NA_dyn_unit_sigma(isigma,isigma_na))
          endif 

          if(write_modes) then
            i_NA_modes_unit_sigma(isigma,isigma_na)=io_file_unit()
            open(unit=i_Na_modes_unit_sigma(isigma,isigma_na),&
              file=trim(adjustl(fil_dyn_ext))//'.NA_modes.pe_'//nd_nmbr//'sig'//postfix(1:6)//'sigNA'//postfix2(1:6),&
              !file=trim(adjustl(fil_dyn_ext))//'.NA_modes.sig'//postfix(1:6)//'sigNA'//postfix2(1:6),&
            status='unknown',form='formatted')
            rewind(i_NA_modes_unit_sigma(isigma,isigma_na))
          endif 



          i_NA_unit_sigma(isigma,isigma_na)=io_file_unit()              
          open(unit=i_NA_unit_sigma(isigma,isigma_na), &
            file=trim(adjustl(fil_dyn_ext))//'.NA_freq.pe_'//nd_nmbr//'sig'//postfix(1:6)//'sigNA'//postfix2(1:6),&
            !file=trim(adjustl(fil_dyn_ext))//'.NA_freq.sig'//postfix(1:6)//'sigNA'//postfix2(1:6),&
          status='unknown',form='formatted')
          rewind(i_NA_unit_sigma(isigma,isigma_na))


        enddo
      enddo


    endif
    !endif

  end subroutine openfile_phonon_freq




  subroutine closefile_phonon_freq()
    use computing_var, only:isigma,i_NA_unit_sigma,i_NA_modes_unit_sigma,&
      i_NA_dyn_unit_sigma,iounit_sigma,isigma_na,iounit2_sigma
    use input_param, only : nsigma,write_modes,write_dyn,&
      broad_lst
    use control_flags, only :  adiabatic,non_adiabatic
    use parallel_var, only: ionode

    implicit none 

    !if(ionode)then
    do isigma=1,size(broad_lst)
      if(adiabatic) then
        if(write_dyn) close(iounit_sigma(isigma))
        close(iounit2_sigma(isigma))
        if(write_modes) close(iounit2_sigma(isigma))
      endif
      if(non_adiabatic) then
        do isigma_na=1,size(broad_lst)
          close(i_NA_unit_sigma(isigma,isigma_na))
          if(write_dyn) close(i_NA_dyn_unit_sigma(isigma,isigma_na))
          if(write_modes) close(i_NA_modes_unit_sigma(isigma,isigma_na))
        enddo
      endif
    enddo
    !end if
  end subroutine closefile_phonon_freq



  subroutine collect_file_phonon_freq()
    use w90_constants, only :ryd2ev
    use computing_var, only:idynunit,ilambdaunit,ilambdavunit
    use io_var, only :  collect_file,delete_file,stdout,exst
    use computing_var, only:isigma,i_NA_unit_sigma,i_NA_modes_unit_sigma,&
      i_NA_dyn_unit_sigma,iounit_sigma,isigma_na,iounit2_sigma,&
      iounit3_sigma,idynunit,postfix,postfix2,degauss2
    use input_param, only : nsigma,write_modes,write_dyn,&
      broad_lst,interp_dyn_file,sigma_min,dsigma,degauss,&
      fil_dyn_ext
    use parallel_var, only: ionode,nproc,nd_str,nd_fmt
    use io_var, only : io_file_unit
    use w90_comms, only : comms_barrier
    use control_flags, only :  adiabatic,non_adiabatic

    integer :: iproc,coll_unit
    character (len=256) :: FNAME_in,FNAME_out


    call comms_barrier()

    if(ionode)then


      if(adiabatic) then
        do isigma=1,nsigma
          degauss=sigma_min+(isigma-1)*dsigma
          degauss=degauss/ryd2eV
          write(postfix(1:12),'(1f12.6)') degauss
          postfix=trim(adjustl(postfix))

          if(write_dyn) then
            FNAME_out=trim(adjustl(fil_dyn_ext))//'.dyn.sigma'//postfix(1:6)
            WRITE(stdout,'(5x,"output file: ",a)')FNAME_out
            call delete_file(FNAME_out)
          endif


          if(write_modes) then
            FNAME_out=trim(adjustl(fil_dyn_ext))//'.modes.sigma'//postfix(1:6)
            WRITE(stdout,'(5x,"output file: ",a)')FNAME_out
            call delete_file(FNAME_out)
          end if

          FNAME_out=trim(adjustl(fil_dyn_ext))//'.freq.sigma'//postfix(1:6)
          WRITE(stdout,'(5x,"output file: ",a)')FNAME_out
          WRITE(stdout,*) 'q-points are in 2pi/alat; frquencies in meV, THz, cm-1'
          
          call delete_file(FNAME_out)

          do iproc=1,nproc
            WRITE( nd_str, nd_fmt ) iproc

            if(write_dyn) then
              FNAME_in= trim(adjustl(fil_dyn_ext))//'.dyn.pe_'//nd_str//'sigma'//postfix(1:6)
              FNAME_out=trim(adjustl(fil_dyn_ext))//'.dyn.sigma'//postfix(1:6)
              INQUIRE( FILE=TRIM(FNAME_in), EXIST=exst )
              IF( exst ) call collect_file(FNAME_in,FNAME_out,0)
              IF( exst ) call delete_file(FNAME_in)
            end if
            if(write_modes) then
              FNAME_in= trim(adjustl(fil_dyn_ext))//'.modes.pe_'//nd_str//'sigma'//postfix(1:6)
              FNAME_out=trim(adjustl(fil_dyn_ext))//'.modes.sigma'//postfix(1:6)
              INQUIRE( FILE=TRIM(FNAME_in), EXIST=exst )
              IF( exst ) call collect_file(FNAME_in,FNAME_out,0)
              IF( exst ) call delete_file(FNAME_in)
            end if
            FNAME_in= trim(adjustl(fil_dyn_ext))//'.freq.pe_'//nd_str//'sigma'//postfix(1:6)
            FNAME_out=trim(adjustl(fil_dyn_ext))//'.freq.sigma'//postfix(1:6)
            INQUIRE( FILE=TRIM(FNAME_in), EXIST=exst )
            IF( exst ) call collect_file(FNAME_in,FNAME_out,0)
            IF( exst ) call delete_file(FNAME_in)
          end do
        enddo
      endif

      if(non_adiabatic) then

        do isigma=1,nsigma
          degauss=sigma_min+(isigma-1)*dsigma
          degauss=degauss/ryd2eV
          write(postfix(1:12),'(1f12.6)') degauss
          postfix=trim(adjustl(postfix))
          do isigma_na=1,size(broad_lst)
            degauss2=broad_lst(isigma_na)
            write(postfix2(1:12),'(1f12.6)') degauss2
            postfix2=trim(adjustl(postfix2))

            if(write_dyn) then
              FNAME_out= trim(adjustl(fil_dyn_ext))//'.NA_dyn.sig'//postfix(1:6)//'sigNA'//postfix2(1:6)
              WRITE(stdout,'(5x,"output file: ",a)')FNAME_out
              call delete_file(FNAME_out)
            endif 
            if(write_modes) then
              FNAME_out= trim(adjustl(fil_dyn_ext))//'.NA_modes.sig'//postfix(1:6)//'sigNA'//postfix2(1:6)
              WRITE(stdout,'(5x,"output file: ",a)')FNAME_out
              call delete_file(FNAME_out)
            endif 
            FNAME_out= trim(adjustl(fil_dyn_ext))//'.NA_freq.sig'//postfix(1:6)//'sigNA'//postfix2(1:6)
            WRITE(stdout,'(5x,"output file: ",a)')FNAME_out
            call delete_file(FNAME_out)

            do iproc=1,nproc
              WRITE( nd_str, nd_fmt ) iproc

              if(write_dyn) then
                FNAME_in= trim(adjustl(fil_dyn_ext))//'.NA_dyn.pe_'//nd_str//'sig'//postfix(1:6)//'sigNA'//postfix2(1:6)
                FNAME_out= trim(adjustl(fil_dyn_ext))//'.NA_dyn.sig'//postfix(1:6)//'sigNA'//postfix2(1:6)
                INQUIRE( FILE=TRIM(FNAME_in), EXIST=exst )
                IF( exst ) call collect_file(FNAME_in,FNAME_out,0)
                IF( exst ) call delete_file(FNAME_in)
              endif 

              if(write_modes) then
                FNAME_in= trim(adjustl(fil_dyn_ext))//'.NA_modes.pe_'//nd_str//'sig'//postfix(1:6)//'sigNA'//postfix2(1:6)
                FNAME_out= trim(adjustl(fil_dyn_ext))//'.NA_modes.sig'//postfix(1:6)//'sigNA'//postfix2(1:6)
                INQUIRE( FILE=TRIM(FNAME_in), EXIST=exst )
                IF( exst ) call collect_file(FNAME_in,FNAME_out,0)
                IF( exst ) call delete_file(FNAME_in)
              endif 

              FNAME_in= trim(adjustl(fil_dyn_ext))//'.NA_freq.pe_'//nd_str//'sig'//postfix(1:6)//'sigNA'//postfix2(1:6)
              FNAME_out= trim(adjustl(fil_dyn_ext))//'.NA_freq.sig'//postfix(1:6)//'sigNA'//postfix2(1:6)
              INQUIRE( FILE=TRIM(FNAME_in), EXIST=exst )
              IF( exst ) call collect_file(FNAME_in,FNAME_out,0)
              IF( exst ) call delete_file(FNAME_in)
            enddo
          enddo
        enddo


      endif

    end if

  end subroutine collect_file_phonon_freq


end Module 
