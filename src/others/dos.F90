!    EPIq v 1.2, Electron-phonon interaction over k and q points
!    Copyright (C) 2022, Matteo Calandra, Gianni Profeta, Francesco Mauri, Giulio Volpato,
!    Nathalie Vast, Jelena Sjakste, Francesco Macheda, Giovanni Marini, Guglielmo Marchese

!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.

!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.

!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.

subroutine dos()
  use utility, only : utility_metric,utility_cart_to_frac
  use io_var, only : io_file_unit, io_time
  use w90_constants, only : pi,twopi,bohr,dp,ryd2ev,cmplx_0
  use wannier_window
  use header
  use kq_points, only : nk_int, xk_int,wk
  IMPLICIT NONE
!
! Constants
!
  REAL(kind=dp), PARAMETER :: eps = 1.0d-6,                        &
                              ryd2cm1 = 13.6058*8065.5,            &
                              rydthz = 13.6058d0 * 241.796d0

!
! Integer variables
!
  LOGICAL ::       have_gamma,count_pts
  LOGICAL ::       lfound
  LOGICAL, allocatable :: compute_k(:)
!
! Integer variables
!
  integer                       :: nrpts,iom
  integer                       :: lstr_i,lstr_j,lstr_format,ngauss,nsigma,iq,nt, nqs_
  INTEGER                       :: i,j,k,l,m,n,mu,nu,ialpha,isigma,icar,jcar,na,nb
  INTEGER                       :: ir,ios, counteri,counterj,counter
  INTEGER                       :: iounit,iounit2
  INTEGER                       :: nk_r,  nbnd_r,nbnd,ngauss_ph
  INTEGER                       :: nk1,nk2,nk3,npoints
  INTEGER                       :: nntot  ! nntot=total number of neighbours per k point

  INTEGER, ALLOCATABLE          :: indexk(:),indexkpq(:)
  integer, ALLOCATABLE          :: irvec(:,:)
  integer, ALLOCATABLE          :: ndegen(:)

!
! Real variables and arrays
!

  REAL(kind=dp)                 :: efermi,dom,omega
  real(kind=dp)                 :: real_lattice(3,3), recip_lattice(3,3), &
                                   real_metric(3,3), recip_metric(3,3)
  REAL(kind=dp)                 :: nel_r,nel,nel_from_input,E_min, E_max
  REAL(kind=dp)                 :: k_int(3)
  REAL(kind=dp)                 :: cart(3),reduced(3),xq_ph(3)
  REAL(kind=dp)                 :: degauss,efermi_calc
  REAL(kind=dp)                 :: w0gauss,wgauss,efermig_monospin,dos_ef  !functions for smearing from PWSCF
  REAL(KIND=DP), ALLOCATABLE    :: mod_R(:)
  REAL(KIND=DP), ALLOCATABLE    :: eig(:,:)
  REAL(kind=dp), ALLOCATABLE    :: kpt_latt(:,:),sum_mod(:),bands(:,:),dosef(:)
  real(kind=dp)                      :: rand

!
! Complex variables and arrays
!

  COMPLEX(kind=dp), ALLOCATABLE :: m_matrix(:,:,:,:)       ! m_matrix in wannierisation

  
!
! Character variables and arrays
!

  CHARACTER*20                  :: prefix,postfix
  CHARACTER*3                   :: band_j
  CHARACTER*20                  :: k_string
  character(len=30)             :: calculation
  character(len=33)             :: header_wan
  character(len=20)             :: checkpoint
  character(len=4)              :: verbosity

  namelist / input_interp / prefix,            &
                            ngauss,            &
                            degauss,           &
                            nel_from_input,    &
                            E_min,E_max,npoints, &
                            verbosity 

  !-----------------------------------------	
  ! Namelist defaults
  !-----------------------------------------	

  ngauss=0
  degauss=0.01
  nel_from_input=-1.0
  E_min=100
  E_max=-100
  npoints=1000
  verbosity='high'
  !-----------------------------------------	
  !          read input..,                 |
  !-----------------------------------------

  read(5,input_interp,err=200,iostat=ios)
  
200 continue
  if(ios.ne.0) then 
     write(6,*) 'error in reading input',ios
     stop
  endif


  !-----------------------------------------
  !write header
  !-----------------------------------------

  call header_k()

  !-----------------------------------------	
  !          read U_mn(k) from wannier     |
  !-----------------------------------------
  iounit=io_file_unit()
  inquire(file=trim(prefix)//'.chk',exist=lfound)

  if(.not.lfound) then
     write(6,*) 'WANNIER90 _chk file is missing, I cannot continue!'
     stop
  endif

  open(unit=iounit,file=trim(adjustl(prefix))//'.chk',status='old',form='unformatted')
  
  read(iounit) header_wan
  read(iounit) num_bands
  read(iounit) num_exclude_bands
  
  allocate( exclude_bands(num_exclude_bands) )
  
  read(iounit) (exclude_bands(i),i=1,num_exclude_bands)
  read(iounit) ((real_lattice(i,j),i=1,3),j=1,3)  ! Real lattice  
  read(iounit) ((recip_lattice(i,j),i=1,3),j=1,3) !reciprocal lattice
  read(iounit) num_kpts
  read(iounit) (mp_grid(i),i=1,3)
  !  read(iounit) num_wann,num_kpts,num_bands
  
  
  ALLOCATE(kpt_all(3,num_kpts)) 
  
  
  read(iounit) ((kpt_all(i,k),i=1,3),k=1,num_kpts)
  
  read(iounit) nntot  ! number of neighbours per k point
  
  read(iounit) num_wann
  
  read(iounit) checkpoint   !to be defined
  checkpoint=adjustl(trim(checkpoint))
  read(iounit) have_disentangled

  if (have_disentangled) then
     
     read(iounit) omega_invariant     ! omega invariant 

     ALLOCATE(ndimwin(num_kpts))
     ALLOCATE(lwindow(num_bands,num_kpts))

     
     read(iounit) ((lwindow(i,k),i=1,num_bands),k=1,num_kpts)
     read(iounit) (ndimwin(k),k=1,num_kpts)
     
     ALLOCATE(u_opt(num_bands,num_wann,num_kpts))
     read(iounit) (((u_opt(i,j,k),i=1,num_bands),j=1,num_wann),k=1,num_kpts)
     
  endif


  ALLOCATE(u_mat(num_wann,num_wann,num_kpts))
  
  read(iounit) (((u_mat(i,j,k),i=1,num_wann),j=1,num_wann),k=1,num_kpts)
  
  
  allocate(m_matrix(num_wann,num_wann,nntot,num_kpts)) 
  read(iounit) ((((m_matrix(i,j,k,l),i=1,num_wann),j=1,num_wann),k=1,nntot),l=1,num_kpts)
  deallocate(m_matrix)
  
  allocate(wannier_centres(3,num_wann))
  read(iounit) ((wannier_centres(i,j),i=1,3),j=1,num_wann) 


  allocate(wannier_spreads(num_wann))
  read(iounit) (wannier_spreads(i),i=1,num_wann)  !to be defined


  close(iounit)
  
  inquire(file=trim(prefix)//'.eig',exist=lfound)

  if(.not. lfound.and.have_disentangled) then
     write(6,*)'No '//trim(prefix)//'.eig file found. Needed for disentanglement!'
     write(6,*) 'Please copy it from wannierisation directory'
     write(6,*) 'and restart the program'
     stop
  elseif(lfound) then
     ALLOCATE(eigval(num_bands,num_kpts))
     iounit=io_file_unit()
     open(unit=iounit,file=trim(prefix)//'.eig',form='formatted',status='old')
     do k=1,num_kpts
        do n=1,num_bands
           read(iounit,*) i,j,eigval(n,k)
           if ((i.ne.n).or.(j.ne.k)) then
              write(6,*) 'param_read: mismatch in '//trim(prefix)//'.eig'
           end if
        enddo
     end do
  endif

  call wannier_report(real_lattice, recip_lattice, num_bands, num_wann, num_kpts, &
     have_disentangled, omega_invariant, wannier_centres, wannier_spreads, 6)

  deallocate(wannier_centres)
  deallocate(wannier_spreads)  

  ! -----------------------------------------
  !   reading k=points for interpolation
  !------------------------------------------
  

  read(5,*) k_string
  !REMEMBER k-points in reduced coordinates.
  if(trim(adjustl(k_string)).eq.'automatic'.or.trim(adjustl(k_string)).eq.'AUTOMATIC') then
     read(5,*) nk1,nk2,nk3
     nk_int=nk1*nk2*nk3
     allocate(xk_int(3,nk_int))
     do i=0,nk1-1
        do j=0,nk2-1
           do k=0,nk3-1
              !  this is nothing but consecutive ordering
              n = k + j*nk3 + i*nk2*nk3 + 1
              !  xkg are the components of the complete grid in crystal axis
              xk_int(1,n) = DBLE(i)/nk1 
              xk_int(2,n) = DBLE(j)/nk2 
              xk_int(3,n) = DBLE(k)/nk3 
           end do
        end do
     end do
  else
     write(6,*) 'Wrong string for k-points, it should be automatic'
     stop
  endif

  




!----------------------------------------------------------------------
!  READING IS FINISHED 
!----------------------------------------------------------------------


  ALLOCATE(kpt_latt(3,num_kpts)) 
  ALLOCATE(u_matrix(num_wann,num_wann,num_kpts))
  ALLOCATE(u_matrix_k(num_wann,num_wann,num_kpts))
  ALLOCATE(u_matrix_opt(num_bands,num_wann,num_kpts))
  ALLOCATE(u_matrix_k_opt(num_bands,num_wann,num_kpts))
  ALLOCATE(indexk(num_kpts))
  ALLOCATE(indexkpq(num_kpts))


!----------------------------------------------------
!      Identify k+q among the k vectors of the mesh |
!----------------------------------------------------
!  call identify_kpq(mp_grid(1),mp_grid(2),mp_grid(3),nk1_small,nk2_small,nk3_small,&
!     indexk,indexkpq,xq_ph)

  xq_ph=0.d0 
  call identify_kpq_general_grid(num_kpts,kpt_all,indexk,indexkpq,xq_ph)


!-------------------------
!   ...just a re-labelling
!-------------------------

  do k=1,num_kpts
      u_matrix_opt(:,:,k)=u_opt(:,:,indexk(k))
      u_matrix_k_opt(:,:,k)=u_opt(:,:,indexkpq(k))

      u_matrix(:,:,k)=u_mat(:,:,indexk(k))
      u_matrix_k(:,:,k)=u_mat(:,:,indexkpq(k))     

      kpt_latt(:,k)=kpt_all(:,indexk(k)) 
  enddo



  have_gamma=.false.
  
  do k=1,num_kpts
     if (all(kpt_latt(:,k)<0.000001)) have_gamma=.true.       
  end do
  if(.not. have_gamma) then
       write(6,'(1x,a)') '!!!! Kpoint grid does not include Gamma. Interpolation is incorrect. !!!!'
       stop
  endif
  
  allocate(irvec(3,3*num_kpts))  
  allocate(ndegen(3*num_kpts))   
  
  
  
  ! Find the number of points in the Wigner-Seitz cell
  
  call utility_metric(real_lattice,recip_lattice,real_metric,recip_metric)
     
  count_pts=.true.
  
  call wigner_seitz_copy(nrpts,num_kpts,ndegen,irvec,mp_grid,real_metric,count_pts)
  

  ! Set up the wigner_seitz vectors and transform Hamiltonian to WF basis
  count_pts=.false.
  call wigner_seitz_copy(nrpts,num_kpts,ndegen,irvec,mp_grid,real_metric,count_pts)



  if(verbosity.eq.'high') then
  !
  !    Calculate |R|
  !
    allocate(mod_R(nrpts))
  
    do ir=1,nrpts
       do i=1,3
          cart(i)=real_lattice(1,i)*irvec(1,ir) + &
                  real_lattice(2,i)*irvec(2,ir) + &
                  real_lattice(3,i)*irvec(3,ir) 
       end do
       mod_R(ir)=sqrt(cart(1)**2+cart(2)**2+cart(3)**2)
    enddo
  endif



  



!--------------------------------------------------
!        Disentangle    the Hamiltonian           |
!-------------------------------------------------
  write(6,*) 'Disentangling hamiltonian ...'
  allocate(eig_ham_wann(num_wann,num_kpts))
  allocate(eig(num_bands,num_kpts))


  if(have_disentangled) then
     
     ! slim down eigval to contain states within the outer window

     do k=1,num_kpts
        counter=0
        do j=1,num_bands
           if(lwindow(j,k)) then
              counter=counter+1
              eig(counter,k)=eigval(j,k)
           end if
        end do
     end do
     
     ! rotate eigval into the optimal subspace
     ! in general eigval would be a matrix at each kpoints
     ! but we choose u_matrix_opt such that the Hamiltonian is
     ! diagonal at each kpoint. (I guess we should check it here)
     
     eig_ham_wann=0.d0
     do k=1,num_kpts
        do i=1,num_wann
           do m=1,ndimwin(indexk(k))
!              eig_ham_wann(i,:)=eig_ham_wann(i,:)+eig(m,:)*Ryd2eV*&
!                   conjg(u_matrix_opt(m,i,:))*u_matrix_opt(m,i,:)
!              eig_ham_wann(i,k)=eig_ham_wann(i,k)+eig(m,k)*&
!                   conjg(u_mat(m,i,k))*u_mat(m,i,k)
                  eig_ham_wann(i,k)=eig_ham_wann(i,k)+eig(m,k)*&
                       conjg(u_opt(m,i,k))*u_opt(m,i,k)

           enddo
        enddo
     enddo
     deallocate(u_opt)
  else
     eig_ham_wann(1:num_wann,:)=eigval(1:num_wann,:)
  endif

  deallocate(lwindow,ndimwin,eigval)
  
  
  
!--------------------------------------------------
!        Transform g_op and H in gw_R and H_R     |
!--------------------------------------------------  
  
  allocate(ham_r(num_wann,num_wann,nrpts))

  write(6,*) 'Performing Fourier transform of H...'

  call plot_get_Hamil(num_kpts,num_wann,nrpts,irvec,ndegen, &
                      eig_ham_wann,kpt_all,u_mat,ham_r)


  write(6,*) '... done  '
   

   

  if(verbosity.eq.'high') then

    iounit=io_file_unit()
  
    open(unit=iounit,file=trim(prefix)//'_h_R.dat',status='unknown',form='formatted')
    rewind(iounit)

    write(iounit,*) '#|R|    Sum_ij |H_ij(R)|^2 '
    do ir=1,nrpts
       reduced=0.d0
       do i=1,num_wann
          do j=1,num_wann
             reduced(1)=reduced(1)+ &
                  sqrt(real(ham_r(i,j,ir)*conjg(ham_r(i,j,ir))))
    
          enddo
       enddo
       write(iounit,*) mod_R(ir), reduced(1)

    enddo
    close(iounit)
  
  

   DEALLOCATE (mod_R)

 endif
  DEALLOCATE(u_mat)
  DEALLOCATE(kpt_all)


  
 
!=====================================================
!            Main loop of interpolation              |
!====================================================  
  
  write(6,*) '==================================='
  write(6,*) '         Begin interpolation       '
  write(6,*) '==================================='
  

     

  write(6,*) '-------------------------------------'
  write(6,*) ' dos calculation        '
  write(6,*) '-------------------------------------'
     
     !
     !  First I need to calculate the Fermi level on the interpolated mesh
     !        (interpolate_bands is identical to get_U_int except that it
     !           does not calculate eigenvectors)
     !


  allocate(dosef(npoints))
  allocate(bands(num_wann,nk_int))
  allocate(compute_k(nk_int))
     

  do k=1,nk_int
     call interpolate_bands(nrpts,ndegen,num_wann,xk_int(1,k),irvec,bands(1,k),ham_r)
  enddo

  
  if(E_min.gt.E_max) then
    
    do i=1,num_wann
       do k=1,nk_int
         if(bands(i,k).lt.E_min) E_min=bands(i,k)
         if(bands(i,k).gt.E_max) E_max=bands(i,k)
       enddo
    enddo

    write(6,*) 'E_min set to ',E_min
    write(6,*) 'E_max set to ',E_max

  endif
     
  write(6,'("Number of electrons set from input=",1f12.6)') nel_from_input
  nel=nel_from_input
  efermi_calc=efermig_monospin(bands,num_wann,nk_int,wk,nel,degauss,ngauss)
  write(6,*) 'The Fermi level (eV) on the dense grid is ',efermi_calc
 
   
  dom=(E_max-E_min)/dfloat(npoints)

  do iom=1,npoints
     omega=E_min+(iom-1)*dom
     dosef(iom)=dos_ef(ngauss, degauss, omega, bands, nk_int, num_wann)/2.0 !per spin
  enddo
  
  
  iounit=io_file_unit()
  open(unit=iounit,file=trim(adjustl(prefix))//'.dos',status='unknown',form='formatted')
 
  write(iounit,*) '# Energy(eV), dos(ev/spin)' 
  do iom=1,npoints
     omega=E_min+(iom-1)*dom
     write(iounit,*) omega,dosef(iom)
  enddo

  close(iounit)

  write(6,*) '==================================='
  write(6,*) '         End interpolation       '
  write(6,*) '==================================='
  
  
  
  

  deallocate(u_matrix)
  deallocate(irvec,ndegen)
  deallocate(kpt_latt)
  deallocate(eig_ham_wann)
  deallocate(xk_int)
  
  
  

end subroutine dos
   
   





!
! in this routine the k-points must enter in crystalline coordinates
!
subroutine identify_kpq_general_grid(num_kpts,kpt_all,indexk,indexkpq,xq)
  use w90_constants, only : dp
  implicit none
  integer :: num_kpts,nk_int
  integer :: indexk(num_kpts),indexkpq(num_kpts)
  real(kind=dp) :: xq(3)
  real(kind=dp) kpt_all(3,num_kpts)
!
  !internal
  logical :: exst
  integer :: i,j,k,n,iqs,iqbig
  real(kind=dp) Gvec(3)

  do n=1,num_kpts
     indexk(n)=n
  enddo

  !
  ! I verify that all the k+q of the small belongs to the grid
  !

  indexkpq=0

  do iqbig=1,num_kpts
     do iqs=1,num_kpts
        if(dabs(kpt_all(1,iqbig)-xq(1)-kpt_all(1,iqs)).lt.1.d-6.and. &
             dabs(kpt_all(2,iqbig)-xq(2)-kpt_all(2,iqs)).lt.1.d-6.and. &
             dabs(kpt_all(3,iqbig)-xq(3)-kpt_all(3,iqs)).lt.1.d-6) then
           indexkpq(iqs)=iqbig
           goto 734
           
        else
           do i=-4,4
              do j=-4,4
                 do k=-4,4
                    Gvec(1)=i+kpt_all(1,iqs)+xq(1)
                    Gvec(2)=j+kpt_all(2,iqs)+xq(2)
                    Gvec(3)=k+kpt_all(3,iqs)+xq(3)
                    if(dabs(kpt_all(1,iqbig)-Gvec(1)).lt.1.d-6.and. &
                         dabs(kpt_all(2,iqbig)-Gvec(2)).lt.1.d-6.and. &
                         dabs(kpt_all(3,iqbig)-Gvec(3)).lt.1.d-6) then
                       indexkpq(iqs)=iqbig
                       goto 734
                    endif
                 enddo
              enddo
           enddo
        endif
     enddo
     write(6,*) 'ERROR,cannot find k+q'
     stop
734 continue
  enddo
  
  
                 


end subroutine identify_kpq_general_grid



subroutine define_band_string(index,string,lstr)
  implicit none
  integer index,lstr
  character*3 string

! here put a check on the string length  

  string=' '
  if(index < 10) then
     WRITE( string(1:1), '(I1)' ) index
     lstr=1
  elseif(index < 100) then
     WRITE( string(1:2), '(I2)' ) index
     lstr=2
  elseif(index < 1000) then
     WRITE( string(1:3), '(I3)' ) index
     lstr=3
  endif
 
  string=trim(adjustl(string))
end subroutine define_band_string


subroutine transf_g_normal_modes_basis(num_wann,nmodes,zz,dyn,zz_scra,g_int,g_mu)
  use w90_constants, only : dp, cmplx_0,cmplx_1
  use utility,    only : utility_zgemm
  implicit none
  integer           ::num_wann,nmodes
  COMPLEX(kind=dp)  :: g_mu(num_wann,num_wann,nmodes)
  complex(kind=dp)  :: g_int(num_wann,num_wann,nmodes)
  complex(kind=dp) :: dyn(nmodes,nmodes), zz(nmodes,nmodes),zz_scra(nmodes,nmodes)
  !internal 
  integer           :: i,j,mu,nu,ialpha


  !
  ! Now we go transform to the normal modes basis
  !

  call utility_zgemm(zz_scra,zz,'C',dyn,'N',nmodes)

!  call zgemm('C','N',nmodes,nmodes,nmodes,cmplx_1,zz,nmodes,dyn,nmodes,&
!       cmplx_0,zz_scra,nmodes)


!  ZGEMM(TRANSA,TRANSB,M,N,K,ALPHA,A,LDA,B,LDB,BETA,C,LDC)
  call zgemm('N','N',num_wann*num_wann,nmodes,nmodes,cmplx_1,g_int(1,1,1),&
       num_wann*num_wann,dyn,nmodes,cmplx_0,g_mu,num_wann*num_wann)

! which is the same as this
! 
!  g_mu=(0.d0,0.d0)
!  do j=1,num_wann
!     do i=1,num_wann
!        do mu=1,nmodes
!           do nu=1,nmodes
!              do ialpha=1,nmodes
!                 g_mu(i,j,mu)=g_mu(i,j,mu)+g_int(i,j,ialpha)*conjg(zz(nu,ialpha))&
!                      *dyn(nu,mu)
!              enddo
!           enddo
!        enddo
!     enddo
!  enddo

end subroutine transf_g_normal_modes_basis

!
! The routine below is never used here but it can be useful
!  (in the case it is needs to be rewritten with dgemm
!
subroutine transf_g_cartesian_basis(num_wann,nmodes,zz,g_int,g_mu)
  use w90_constants, only : dp, cmplx_0,cmplx_1
  use utility,    only : utility_zgemm
  implicit none
  integer           ::num_wann,nmodes
  COMPLEX(kind=dp)  :: g_mu(num_wann,num_wann,nmodes)
  complex(kind=dp)  :: g_int(num_wann,num_wann,nmodes)
  complex(kind=dp) ::  zz(nmodes,nmodes)
  !internal 
  integer           :: i,j,mu,nu,ialpha


  !
  ! Now we go transform to the normal modes basis
  !

  g_mu=(0.d0,0.d0)
  do j=1,num_wann
     do i=1,num_wann
        do mu=1,nmodes
           do ialpha=1,nmodes
              g_mu(i,j,mu)=g_mu(i,j,mu)+g_int(i,j,ialpha)*conjg(zz(nu,ialpha))
           enddo
        enddo
     enddo
  enddo

!
! optimization with dgemv possible
!

end subroutine transf_g_cartesian_basis


