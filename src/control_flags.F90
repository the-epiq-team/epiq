!    EPIq v 1.2, Electron-phonon interaction over k and q points
!    Authors : Matteo Calandra, Gianni Profeta, Francesco Mauri, Giulio Volpato,
!    Nathalie Vast, Jelena Sjakste, Francesco Macheda, Giovanni Marini, Guglielmo Marchese

!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.

!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.

!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.


Module control_flags
  logical :: adiabatic, non_adiabatic
  logical :: have_gamma
  logical :: count_pts, count_pts_q
  LOGICAL, allocatable :: compute_k(:)
  LOGICAL :: do_commutator=.false.
end Module control_flags
