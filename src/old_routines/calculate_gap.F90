!    EPIq v 1.2, Electron-phonon interaction over k and q points
!    Authors : Matteo Calandra, Gianni Profeta, Francesco Mauri, Giulio Volpato,
!    Nathalie Vast, Jelena Sjakste, Francesco Macheda, Giovanni Marini, Guglielmo Marchese

!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.

!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.

!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.


program calculate_gap
  implicit none           
  integer, parameter          :: dp = selected_real_kind(15,300)
  complex(kind=dp), parameter :: cmplx_0 = (0.0_dp,0.0_dp)
  logical :: lkonFS, gap_plot_matsubara
  logical,allocatable :: wann_on_Fsurf(:)
  integer :: num_wann, nmatsu,nk_int, idummy, nsigma, nq_int
  integer :: iuin, iuout,i,j,k,iom,is, ios, nk_tot_gen, ik_on_FS
  real(kind=dp) :: theta, mustar, efermi, sigma_min, sigma_max, sigma
  real(kind=dp) :: dsigma, gap_threashold,gap_threashold_, gap_threashold_eV
  real(kind=dp) :: weight
  real(kind=dp) :: rdummy(3),imdummy(3)
  real(kind=dp) , allocatable :: xk_int(:,:), bands(:,:), om_matsu(:),dosef(:)
  complex(kind=dp), allocatable :: z(:,:,:), phi(:,:,:), gap(:,:,:)
  complex(kind=dp) , allocatable :: average_gap(:,:)
  character(LEN=256) :: kline
  character(LEN=256) :: inputfile, outputfile
! functions
  real(kind=dp) :: w0gauss, dos_ef
  
  namelist / input_gap / sigma_min, sigma_max, nsigma, inputfile, outputfile, gap_threashold, &
       gap_plot_matsubara

  gap_threashold=-10.0

  read(5,input_gap,err=200,iostat=ios)
 
200 continue
  if(ios.ne.0) then
     write(6,*) 'error in reading input',ios
     stop
  endif

  iuin=10
  open(unit=iuin,file=trim(adjustl(inputfile)),status='unknown',form='formatted')
  iuout=20
  open(unit=iuout,file=trim(adjustl(outputfile)),status='unknown',form='formatted')

  read(iuin,'(1a37, 1i4)') kline(1:37), num_wann
  write(6,*) kline(1:37) 
  if(kline(1:33).ne.'  #   Number of Wannier functions') then
     write(6,*) 'Not a Migdal Eliashberg file, program is stopped'
  endif

  read(iuin,*)
  read(iuin,*) nk_int, nq_int, nk_tot_gen
  read(iuin,*)
  read(iuin,*) nmatsu, theta, mustar, efermi, gap_threashold_
  read(iuin,*)

  write(6,*) 'Number of Wannier functions ',num_wann
  write(6,*) 'Number of k-points  ', nk_tot_gen
  write(6,*) '    of which ',nk_int,' on the Fermi surface'
  write(6,*) 'Number of Matsubara frequencies ', nmatsu
  write(6,*) nsigma,' smearings between ', sigma_min, ' and ', sigma_max

  if(gap_threashold.lt.0.d0) then
     gap_threashold_eV=gap_threashold_
  else
     gap_threashold_eV=gap_threashold/1000.d0
  endif

  allocate(xk_int(3,nk_int))
  allocate(bands(num_wann,nk_int))
  allocate(wann_on_Fsurf(num_wann))
  allocate(z(num_wann,nmatsu,nk_int))
  allocate(phi(num_wann,nmatsu,nk_int))
  allocate(gap(num_wann,nmatsu,nk_int))
  allocate(average_gap(num_wann,nmatsu))
  allocate(om_matsu(nmatsu))
  allocate(dosef(nsigma))

  z=cmplx_0
  gap=cmplx_0
  phi=cmplx_0

  if(gap_plot_matsubara) then
     open(unit=30,file='gap_plot.d',status='unknown',form='formatted')
  endif

  ik_on_FS=0
  do k=1, nk_int
     read(iuin,*) (xk_int(j,k), j=1,3), lkonFS
     read(iuin,'(30L8)') (wann_on_Fsurf(j), j=1,num_wann)
     do i=1,num_wann
        read(iuin,*) idummy, bands(i,k)
        do iom=1,nmatsu
           read(iuin,*) om_matsu(iom),rdummy(1),imdummy(1),rdummy(2),imdummy(2), &
                rdummy(3),imdummy(3)
!          if(wann_on_Fsurf(i)) then
              z(i,iom,k)=cmplx(rdummy(1),imdummy(1))
              phi(i,iom,k)=cmplx(rdummy(2),imdummy(2))
              gap(i,iom,k)=cmplx(rdummy(3),imdummy(3))
!          endif
          

        enddo
     enddo
  enddo


  if(nsigma.gt.1) then
     dsigma=(sigma_max-sigma_min)/(nsigma-1)
  else
     dsigma=0.d0
  endif

  !
  !Calculate dos
  !  

  do is=1, nsigma
     sigma=sigma_min+(is-1)*dsigma
     dosef(is)=dos_ef(0, sigma, efermi, bands, nk_int, num_wann)&
          *real(nk_int,dp)/2.0/real(nk_tot_gen,dp) !per spin
     write(6,*) 'dosef(',sigma,')=',dosef(is)
  enddo

  !
  !Calculate average gap
  !


     
  do is=1, nsigma
     sigma=sigma_min+(is-1)*dsigma
     average_gap=cmplx_0
     do k=1, nk_int
        do i=1,num_wann
           weight=w0gauss( (bands(i, k) - efermi  ) / sigma , 0) &
                / sigma / real(nk_tot_gen, dp)/dosef(is)
           do iom=1,nmatsu
              
              if(real(gap(i,iom,k))*weight.gt.gap_threashold_eV.and.gap_plot_matsubara) then
                 
                 write(30,'(3f22.12)') om_matsu(iom),real(gap(i,iom,k),dp)*weight, &
                      aimag(gap(i,iom,k))*weight
              endif
              if(iom.eq.1) write(77,'(3f22.12)') om_matsu(iom), gap(i,iom, k)*weight
              average_gap(i,iom)=average_gap(i,iom)+&
                   gap(i,iom,k)*weight
              
           enddo
        enddo
     enddo
     write(iuout,'("# Smearing(eV) = ", 1F16.8)') sigma
     write(iuout,'("# Gap at minimal matsubara frequency is")')
     
     do i=1,num_wann
        write(iuout,'("# gap wannier ",1i4, 2F22.12)') i, average_gap(i,1)
     enddo
     do i=1,num_wann
        write(iuout,'("# wannier function number ",1i4)') i
        do iom=1,nmatsu
           write(iuout,'(3F22.12)') om_matsu(iom), average_gap(i,iom)
        enddo
        write(iuout,*)
     enddo
     write(iuout,*)
  enddo

  
  close(30)

  close(iuin)
  close(iuout)

  deallocate(dosef)
  deallocate(om_matsu)
  Deallocate(xk_int)
  deallocate(wann_on_Fsurf)
  deallocate(bands)
  deallocate(z, phi, gap)
  deallocate(average_gap)

end program calculate_gap
