!    EPIq v 1.2, Electron-phonon interaction over k and q points
!    Authors : Matteo Calandra, Gianni Profeta, Francesco Mauri, Giulio Volpato,
!    Nathalie Vast, Jelena Sjakste, Francesco Macheda, Giovanni Marini, Guglielmo Marchese

!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.

!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.

!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.


program  generate_kgrid
  USE w90_constants, ONLY: DP
  IMPLICIT NONE
  INTEGER  :: k1, k2, k3, nk1, nk2, nk3
  INTEGER  :: nks, iseed
  REAL(DP) :: bg(3,3), xcartph(3)
  REAL(DP), ALLOCATABLE :: xk(:,:), wk(:) 
! internal
  INTEGER :: ik,j,i

  iseed=473989
  read(5,*) nk1, nk2, nk3, k1, k2, k3
  read(5,*) bg(1,1),bg(2,1),bg(3,1)
  read(5,*) bg(1,2),bg(2,2),bg(3,2)
  read(5,*) bg(1,3),bg(2,3),bg(3,3)
  read(5,*) iseed

  allocate(wk(nk1*nk2*nk3))
  allocate(xk(3,nk1*nk2*nk3))


  call kpoint_grid_nosym_r(k1,k2,k3,nk1,nk2,nk3,nks,xk,wk,iseed)

! write(6,*) 'K_POINTS  crystal'
! write(6,*) nks   
! do ik=1,nks
!   write(6,'(4f16.12)') xk(1,ik),xk(2,ik),xk(3,ik),wk(ik)
! enddo

! stop

  do j=1,nks
    do i=1,3
      xcartph(i)=xk(1,j)*bg(i,1)   &
         +xk(2,j)*bg(i,2)+xk(3,j)*bg(i,3)
    enddo
    do i=1,3
      xk(i,j)=xcartph(i)
    enddo
  enddo


  write(6,*) 'K_POINTS  tpiba'
  write(6,*) nks   
  do ik=1,nks
    write(6,'(4f16.12)') xk(1,ik),xk(2,ik),xk(3,ik),wk(ik)
  enddo


 
  deallocate(wk)
  deallocate(xk)


end program generate_kgrid

!This routine is adapted from Quantum ESPRESSO
!
!-----------------------------------------------------------------------
SUBROUTINE kpoint_grid_nosym_r (k1,k2,k3, nk1,nk2,nk3, nks, xk, wk, iseed)
!-----------------------------------------------------------------------
!
!  Automatic generation of a uniform grid of k-points
!
  USE w90_constants, ONLY: DP
  IMPLICIT NONE
  !
  INTEGER, INTENT(in)::  k1, k2, k3, nk1, nk2, nk3
! real(DP), INTENT(in):: bg(3,3)
  !
  INTEGER, INTENT(out) :: nks
  real(DP), INTENT(out):: xk(3,nk1*nk2*nk3)
  real(DP), INTENT(out):: wk(nk1*nk2*nk3)
  ! LOCAL:
  real(DP) :: drand1
  real(DP) :: xkr(3), fact, sh(3)
  INTEGER :: nkr, i,j,k, ns, n, nk, npk,iseed
  INTEGER, ALLOCATABLE :: equiv(:)
  !
  
  nks=nk1*nk2*nk3
  wk=1.d0/real(nks,dp) 

  if(k1.lt.0.or.k2.lt.0.or.k3.lt.0) then
     write(6,*) 'Adding a random shift to the k-points'
     call rand_init(iseed)
     write(6,*) 'using iseed=',iseed
     if(k1.lt.0) sh(1)=drand1()
     if(k2.lt.0) sh(2)=drand1()
     if(k3.lt.0) sh(3)=drand1()
  else
     sh=0.d0
  endif
  
  
  if(k1.lt.0.or.k2.lt.0.or.k3.lt.0) then
     do i=1,nk1
        do j=1,nk2
           do k=1,nk3
              !  this is nothing but consecutive ordering
              n = (k-1) + (j-1)*nk3 + (i-1)*nk2*nk3 + 1
              xk(1,n) = DBLE(i-1)/nk1 + sh(1)/2.0/nk1
              xk(2,n) = DBLE(j-1)/nk2 + sh(2)/2.0/nk2
              xk(3,n) = DBLE(k-1)/nk3 + sh(3)/2.0/nk3
           end do
        end do
     end do
  else  
     DO i=1,nk1
        DO j=1,nk2
           DO k=1,nk3
           !  this is nothing but consecutive ordering
             n = (k-1) + (j-1)*nk3 + (i-1)*nk2*nk3 + 1
           !  xk are the components of the complete grid in crystal axis
             xk(1,n) = dble(i-1)/nk1 + dble(k1)/2.d0/nk1
             xk(2,n) = dble(j-1)/nk2 + dble(k2)/2.d0/nk2
             xk(3,n) = dble(k-1)/nk3 + dble(k3)/2.d0/nk3
          ENDDO
        ENDDO
     ENDDO
  endif


  RETURN
END SUBROUTINE kpoint_grid_nosym_r

