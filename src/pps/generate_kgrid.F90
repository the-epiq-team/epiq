!    EPIq v 1.2, Electron-phonon interaction over k and q points
!    Authors : Matteo Calandra, Gianni Profeta, Francesco Mauri, Giulio Volpato,
!    Nathalie Vast, Jelena Sjakste, Francesco Macheda, Giovanni Marini, Guglielmo Marchese

!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.

!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.

!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.


program  generate_kgrid
  USE w90_constants, ONLY: DP
  IMPLICIT NONE
  INTEGER  :: k1, k2, k3, nk1, nk2, nk3
  INTEGER  :: nks
  REAL(DP), ALLOCATABLE :: xk(:,:), wk(:) 
! internal
  INTEGER :: ik

  read(5,*) nk1, nk2, nk3, k1, k2, k3

  allocate(wk(nk1*nk2*nk3))
  allocate(xk(3,nk1*nk2*nk3))


  call kpoint_grid_nosym(k1,k2,k3,nk1,nk2,nk3,nks,xk,wk)

  write(6,*) 'K_POINTS  crystal'
  write(6,*) nks   
  do ik=1,nks
    write(6,'(4f16.12)') xk(1,ik),xk(2,ik),xk(3,ik),wk(ik)
  enddo


 
  deallocate(wk)
  deallocate(xk)


end program generate_kgrid

! This subroutine is adapted from Quantum ESPRESSO
!
!-----------------------------------------------------------------------
SUBROUTINE kpoint_grid_nosym (k1,k2,k3, nk1,nk2,nk3, nks, xk, wk)
!-----------------------------------------------------------------------
!
!  Automatic generation of a uniform grid of k-points
!
  USE w90_constants, ONLY: DP
  IMPLICIT NONE
  !
  INTEGER, INTENT(in)::  k1, k2, k3, nk1, nk2, nk3
! real(DP), INTENT(in):: bg(3,3)
  !
  INTEGER, INTENT(out) :: nks
  real(DP), INTENT(out):: xk(3,nk1*nk2*nk3)
  real(DP), INTENT(out):: wk(nk1*nk2*nk3)
  ! LOCAL:
  real(DP) :: xkr(3), fact
  real(DP), ALLOCATABLE:: xkg(:,:), wkk(:)
  INTEGER :: nkr, i,j,k, ns, n, nk, npk
  INTEGER, ALLOCATABLE :: equiv(:)
  !
  nkr=nk1*nk2*nk3
  npk=nkr
  ALLOCATE (xkg( 3,nkr),wkk(nkr))
  ALLOCATE (equiv( nkr))
  !
  DO i=1,nk1
     DO j=1,nk2
        DO k=1,nk3
           !  this is nothing but consecutive ordering
           n = (k-1) + (j-1)*nk3 + (i-1)*nk2*nk3 + 1
           !  xkg are the components of the complete grid in crystal axis
           xkg(1,n) = dble(i-1)/nk1 + dble(k1)/2/nk1
           xkg(2,n) = dble(j-1)/nk2 + dble(k2)/2/nk2
           xkg(3,n) = dble(k-1)/nk3 + dble(k3)/2/nk3
        ENDDO
     ENDDO
  ENDDO

  !  equiv(nk) =nk : k-point nk is not equivalent to any previous k-point
  !  equiv(nk)!=nk : k-point nk is equivalent to k-point equiv(nk)

  DO nk=1,nkr
     equiv(nk)=nk
  ENDDO


  wkk = 1.d0

  !  count irreducible points and order them

  nks=0
  fact=0.0d0
  DO nk=1,nkr
     IF (equiv(nk)==nk) THEN
        nks=nks+1
        IF (nks>npk) then
            write(6,*) 'too many k-points'
            stop
        ENDIF
        wk(nks) = wkk(nk)
        fact    = fact+wk(nks)
        !  bring back into to the first BZ
        DO i=1,3
           xk(i,nks) = xkg(i,nk) !-nint(xkg(i,nk)) !removed for consistency
        ENDDO
     ENDIF
  ENDDO
  !  go to cartesian axis (in units 2pi/a0)
!  CALL cryst_to_cart(nks,xk,bg,1)
  !  normalize weights to one
  DO nk=1,nks
     wk(nk) = wk(nk)/fact
  ENDDO

  DEALLOCATE(equiv)
  DEALLOCATE(xkg,wkk)

  RETURN
END SUBROUTINE kpoint_grid_nosym

