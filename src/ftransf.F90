!    EPIq v 1.2, Electron-phonon interaction over k and q points
!    Authors : Matteo Calandra, Gianni Profeta, Francesco Mauri, Giulio Volpato,
!    Nathalie Vast, Jelena Sjakste, Francesco Macheda, Giovanni Marini, Guglielmo Marchese

!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.

!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.

!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.

!This module contains code adapted from wannier90
module ftransf_module
contains





subroutine Ftransf_H_ws_no_paral(k_int, irvec, ndegen,nrpts,Or,Ok,vel_k)
  use w90_constants, only : dp,twopi, cmplx_i, bohr,ryd2ev, pi
  use w90_ws_distance, only: irdist_ws, wdist_ndeg, ws_translate_dist
  use wannier_window, only: num_wann, num_kpts
  use input_param, only:  real_lattice,recip_lattice,use_ws_distance
  use computing_var, only : at_local,bg_local,celldm_local
  use utility, only : utility_frac_to_cart
 

  implicit none
  integer :: nrpts,ir
  integer           :: i,j, kind,n,nmod
  integer :: irvec(3,3*num_kpts),ndegen(3*num_kpts)

  integer          :: ideg
  real(kind=dp)    :: rdotk
  real(kind=dp) :: k_int(3),crvec(3)

  complex(kind=dp) :: Fphase
  complex(kind=dp) :: Or(num_wann,num_wann,nrpts), Ok(num_wann,num_wann)
  complex(kind=dp),optional :: vel_k(3,num_wann,num_wann)

  !kind.eq.1 == r--->k
  if (use_ws_distance)  then! [lp] Shift the WF to have the minimum distance IJ, see also ws_distance.F90
    !CALL ws_translate_dist(nrpts, irvec,.true.)
    CALL ws_translate_dist(nrpts, irvec)
    do ir = 1, nrpts
      call utility_frac_to_cart(real(irvec(:, ir),dp),crvec(:),recip_lattice)

      do i = 1, num_wann
        do j = 1, num_wann
          do ideg = 1, wdist_ndeg(i, j, ir)
            rdotk = twopi*dot_product(k_int(:), real(irdist_ws(:, ideg, i, j, ir), dp))
            Fphase = cmplx(cos(rdotk), sin(rdotk), dp)/real(ndegen(ir)*wdist_ndeg(i, j, ir), dp)
            Ok(i, j) = Ok(i, j) + Fphase*Or(i, j, ir)
            if ( present(vel_k) ) then
              do n=1,3
                vel_k(n,i, j) = vel_k(n,i, j)  + cmplx_i*crvec(n)*Fphase*Or(i, j, ir) !cmplx_i*crvec(n)*Fphase*Or(i, j, ir) 
              enddo
              !call v_cart_to_crys(vel_k(:,i, j))
            endif
          enddo
        enddo
      enddo
    enddo
  else  ! [lp] Original code, without IJ-dependent shift:
    do ir = 1, nrpts
      call utility_frac_to_cart(real(irvec(:, ir),dp),crvec(:),recip_lattice)

      rdotk = twopi*dot_product(k_int(:), irvec(:, ir))
      Fphase = cmplx(cos(rdotk), sin(rdotk), dp)/real(ndegen(ir), dp)
      Ok(:, :) = Ok(:, :) + Fphase*Or(:, :, ir)
      if ( present(vel_k) ) then
        do i = 1, num_wann
          do j = 1, num_wann
            do n=1,3
              vel_k(n,i, j) = vel_k(n,i, j)  +  cmplx_i*crvec(n)*Fphase*Or(i, j, ir) 
            enddo
          enddo
        enddo
      endif
    enddo
  endif

  if ( present(vel_k) ) &
    vel_k = vel_k/ryd2ev 
end subroutine Ftransf_H_ws_no_paral

subroutine Ftransf_ws_no_paral(k_int, irvec, ndegen,nrpts,nmod,Or,Ok)
  use w90_constants, only : dp,twopi
  use w90_ws_distance, only: irdist_ws, wdist_ndeg, ws_translate_dist
  use wannier_window, only:  num_wann, num_kpts
  use input_param, only: use_ws_distance



  implicit none
  integer :: nrpts,ir
  integer           :: i,j, kind,n,nmod
  integer :: irvec(3,3*num_kpts),ndegen(3*num_kpts)

  integer          :: ideg
  real(kind=dp)    :: rdotk
  real(kind=dp) :: k_int(3)

  complex(kind=dp) :: Fphase
  complex(kind=dp) :: Or(num_wann,num_wann,nmod,nrpts), Ok(num_wann,num_wann,nmod)

  !kind.eq.1 == r--->k
  if (use_ws_distance)  then! [lp] Shift the WF to have the minimum distance IJ, see also ws_distance.F90
    !CALL ws_translate_dist(nrpts, irvec,.true.)
    CALL ws_translate_dist(nrpts, irvec)
    do ir = 1, nrpts
      do i = 1, num_wann
        do j = 1, num_wann
          do ideg = 1, wdist_ndeg(i, j, ir)
            rdotk = twopi*dot_product(k_int(:), real(irdist_ws(:, ideg, i, j, ir), dp))
            Fphase = cmplx(cos(rdotk), sin(rdotk), dp)/real(ndegen(ir)*wdist_ndeg(i, j, ir), dp)
            !Fphase = cmplx(cos(rdotk), sin(rdotk), dp)/real(wdist_ndeg(i, j, ir), dp)
            do n = 1, nmod
              Ok(i, j, n) = Ok(i, j, n) + Fphase*Or(i, j,n, ir)
            enddo
          enddo
        enddo
      enddo
    enddo
  else  ! [lp] Original code, without IJ-dependent shift:
    do ir = 1, nrpts
      rdotk = twopi*dot_product(k_int(:), irvec(:, ir))
      Fphase = cmplx(cos(rdotk), sin(rdotk), dp)/real(ndegen(ir), dp)
      do n = 1, nmod
        Ok(:, :, n) = Ok(:, :, n) + Fphase*Or(:, :,n, ir)
      enddo
    enddo
  endif

end subroutine Ftransf_ws_no_paral


subroutine Ftransf_ws_no_paral_r_R(k_int,xq_int,irvec,irvec_q, ndegen,ndegen_q,nrpts,nrpts_q,nmod,nq,Or,Ok)
  use w90_constants, only : dp,twopi
  use w90_ws_distance, only: irdist_ws, wdist_ndeg, ws_translate_dist
  use w90_ws_distance_q, only: irdist_ws_q, wdist_ndeg_q, ws_translate_dist_q
  use wannier_window, only:  num_wann, num_kpts
  use input_param, only: use_ws_distance



  implicit none
  integer :: nrpts,ir,nrpts_q,irq
  integer           :: i,j, kind,n,nmod,nq
  integer :: irvec(3,3*num_kpts),ndegen(3*num_kpts)
  integer :: irvec_q(3,3*nq),ndegen_q(3*nq)

  integer          :: ideg,idegq
  real(kind=dp)    :: rdotk,rdotq
  real(kind=dp) :: k_int(3),xq_int(3)

  complex(kind=dp) :: Fphase,Fphaseq
  complex(kind=dp) :: Or(num_wann,num_wann,nmod,nrpts,nrpts_q), Ok(num_wann,num_wann,nmod)

  !kind.eq.1 == r--->k
    !CALL ws_translate_dist(nrpts, irvec,.true.)
    CALL ws_translate_dist(nrpts, irvec)
    CALL ws_translate_dist_q(nrpts_q, irvec_q)
   do irq = 1,nrpts_q
    do ir = 1, nrpts
      do i = 1, num_wann
        do j = 1, num_wann
          do ideg = 1, wdist_ndeg(i, j, ir)
          do idegq = 1, wdist_ndeg_q(i, j, irq)
            rdotk = twopi*dot_product(k_int(:), real(irdist_ws(:, ideg, i, j, ir), dp))
            Fphase = cmplx(cos(rdotk), sin(rdotk), dp)/real(ndegen(ir)*wdist_ndeg(i, j, ir), dp)
            rdotq = twopi*dot_product(xq_int(:), real(irdist_ws_q(:, ideg, i, j, irq), dp))
            Fphaseq = cmplx(cos(rdotq), sin(rdotq), dp)/real(ndegen(irq)*wdist_ndeg_q(i, j, irq), dp)
            !Fphase = cmplx(cos(rdotk), sin(rdotk), dp)/real(wdist_ndeg(i, j, ir), dp)
            do n = 1, nmod
              Ok(i, j, n) = Ok(i, j, n) + Fphase*Fphaseq*Or(i, j,n, ir,irq)
            enddo
          enddo
        enddo
      enddo
    enddo
   enddo
  enddo

end subroutine Ftransf_ws_no_paral_r_R


end module ftransf_module
