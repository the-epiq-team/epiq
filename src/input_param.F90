!    EPIq v 1.2, Electron-phonon interaction over k and q points
!    Authors : Matteo Calandra, Gianni Profeta, Francesco Mauri, Giulio Volpato,
!    Nathalie Vast, Jelena Sjakste, Francesco Macheda, Giovanni Marini, Guglielmo Marchese

!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.

!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.

!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.

Module input_param
  use w90_constants, only :dp
  implicit none
  logical                           :: acoustic_sum_rule,old_wannier_format
  INTEGER, PARAMETER            :: ntyp_max=200
  LOGICAL                       :: interpolate_k,dump_gR,read_dumped_gR
  LOGICAL                       :: bandres_lw,out2json
  LOGICAL                       :: post_processing,main_drive
  LOGICAL                       :: fdos_only
  LOGICAL                       :: write_modes, write_dyn, Ef_from_input
  LOGICAL                       :: read_modes, Fourier_interp_dyn
  LOGICAL                       :: ordered_dyn_file
  LOGICAL                       :: defects,diel,holes
  LOGICAL                       :: ascii_G_and_H
  LOGICAL                       :: use_alternative_dyn,forceq_dot5
  LOGICAL                       :: system2d
  LOGICAL                       :: fet
  LOGICAL                       :: write_bands
  logical                       :: use_ws_distance,use_ws_distance_elph
  INTEGER                       :: ngauss,laser_nfreq,ngauss_ph,nsigma
  INTEGER                       :: laser_nbroad,iseed,Nqlt
  integer                       :: mp_grid_q(3),nq1,nq2,nq3,wan_start,wan_end,wan_lt
  INTEGER                       :: nfreq, nbroad
  INTEGER                       :: atoms_nmbr
  REAL(kind=dp)                 :: freq_min,freq_max, broad_min,broad_max
  REAL(kind=dp)                 :: sigma_min,sigma_max
  REAL(kind=dp)                 :: theta
  REAL(kind=dp)                 :: laser_freq_min
  !! first frequency (eV) of the impinging laser
  REAL(kind=dp)                 :: laser_freq_max
  !! last frequency (eV) of the impinging laser
  REAL(kind=dp)                 :: thr_compute_k,efermi,chosen_sigma
  REAL(kind=dp)                 :: sigma_ph
  REAL(kind=dp)                 :: laser_broad_min, laser_broad_max
  REAL(kind=dp)                 :: alternative_mass(ntyp_max)
  REAL(kind=dp)                 :: sigma,dsigma
  REAL(kind=dp)                 :: sigma_min_ryd, sigma_max_ryd, dsigma_ryd
  REAL(kind=dp)                 :: conv_factor_lw,beta,degauss
  REAL(kind=dp)                 :: real_lattice(3,3), recip_lattice(3,3), &
    real_metric(3,3), recip_metric(3,3)

  REAL(kind=dp)                 :: nel_r,homo,lumo
  REAL(kind=dp),allocatable     :: freq_lst(:),broad_lst(:)
  CHARACTER(LEN=256)            :: freq_broad_units

  CHARACTER(len=20)             :: prefix='epik'
  character(len=30)             :: calculation
  character(len=4)              :: verbosity
  CHARACTER(LEN=256)            :: ph_irr_dyn_prefix
  character(len=256)            :: interp_mode_file,gR_filename
  character(len=256)            :: interp_dyn_file,fil_dyn_ext,diel_file
  character(len=256)            :: prefix_alt_dyn,commutator_file,diff_start_file
  character(len=256)            :: elphmat_dir

  !DR_Raman
  INTEGER                       :: modes_min,drr_pp_nproc
  LOGICAL                       :: gw_correction
  !!  effective GW correction to graphene Dirac cone
  LOGICAL                       :: compute_gammak
  !!  computes the electron linewidth for each k point, Eq. 8 of Phys. Rev. B 84, 035433
  REAL(kind=dp)                 :: factgamma
  !! factor for inverse lifetime in raman response
  character(len=30)             :: raman_type
  character(len=128)             :: q_points_file
  !DR_Raman

  namelist / control / prefix,&
    verbosity, &
    calculation,theta,&
    fet,&
    read_dumped_gR, &
    dump_gR, &
    ascii_G_and_H,&
    iseed,&
    post_processing,main_drive,&
    old_wannier_format,&
    diff_start_file,elphmat_dir, &
    use_ws_distance,&
    use_ws_distance_elph,out2json,&
    freq_min,freq_max,nfreq,&
    broad_min,broad_max,nbroad,freq_broad_units,write_bands

  namelist / electrons / theta,ngauss,sigma_min,sigma_max,nsigma,efermi,Ef_from_input,wan_start,&
    wan_end,thr_compute_k,commutator_file,wan_lt,holes,nel_r,fdos_only

  namelist / phonons / write_modes,write_dyn,fil_dyn_ext,interp_mode_file,interp_dyn_file,&
    ordered_dyn_file,&
    acoustic_sum_rule,Fourier_interp_dyn,prefix_alt_dyn,ph_irr_dyn_prefix,&
    read_modes,use_alternative_dyn,alternative_mass,diel,&
    nq1,nq2,nq3,Nqlt,q_points_file,forceq_dot5,ngauss_ph,sigma_ph,bandres_lw, atoms_nmbr    

  namelist / resonant_raman /modes_min,factgamma,gw_correction,raman_type, &
    laser_freq_min,laser_freq_max,laser_nfreq,laser_nbroad,laser_broad_min,&
    laser_broad_max,compute_gammak, system2d, drr_pp_nproc

  namelist / diff_start_param /ngauss_ph,sigma_ph,efermi,nel_r,homo,lumo 

contains

  subroutine init_and_read_input()
    use io_var, only : file_in, stdout,input_p_unit,ios,error,&
      exst
    use migdal, only : input_migdal
    use io_var, only : io_file_unit
    use wannier_window
    use w90_constants, only : twopi,bohr
    use espresso_dynamical_matrix, only : xq,celldm,nat,nmodes
    use kq_points, only : nq,nk_int_ph,nq_int
    use computing_var, only :i,j,at_local,bg_local
    use parallel_var,only: nproc,pe_id,ionode
    implicit none
    character(len=999)             :: line

    character (len=30) :: listread="" 

    integer:: diff_start_param_iu

    call input_namelist_defaults()


    if(file_in.eq."") then
      call error('input_param','no input file',1)
      stop
    end if
    ios=0
    input_p_unit=io_file_unit()
    open(unit=input_p_unit,file=trim(adjustl(file_in)),status='unknown',form='formatted')


    listread = "control" 
    read(input_p_unit,control,err=200,iostat=ios)
    listread = "electrons" 
    read(input_p_unit,electrons,err=200,iostat=ios)
    listread = "phonons" 
    read(input_p_unit,phonons,err=200,iostat=ios)
    if(trim(calculation).eq.'resonant_raman')then
      listread = "resonant_raman" 
      read(input_p_unit,resonant_raman,err=200,iostat=ios)
    end if
    !200 continue
    !if(ios.ne.0) then 
    !write(stdout,*) 'error in reading input: ',trim(listread),' ios=',ios
    !stop
    !endif



    200 continue
    if (ios/=0) then
      backspace(input_p_unit)
      read(input_p_unit,fmt='(A)') line
      call error('init_and_read_input','Error in reading input: '//trim(listread)//'. Invalid line : '//trim(line),ios) 
    end if

    diff_start_file=trim(adjustl(diff_start_file))
    if(diff_start_file.ne." ") then
      !print*,'aa',diff_start_file,'aa'
      diff_start_param_iu=io_file_unit()
      open(unit=diff_start_param_iu,file=diff_start_file,status='unknown',form='formatted')
      listread = "diff_start_param" 
      read(diff_start_param_iu,diff_start_param,iostat=ios)
      if (ios/=0) then
        backspace(diff_start_param_iu)
        read(diff_start_param_iu,fmt='(A)') line
        call error('init_and_read_input','Error in reading '//diff_start_file//': '//trim(listread)//&
          '. Invalid line : '//trim(line),ios) 
      end if

      ! read again input file: in this way user can override option read in file
      rewind(input_p_unit)
      listread = "control" 
      read(input_p_unit,control,err=200,iostat=ios)
      listread = "electrons" 
      read(input_p_unit,electrons,err=200,iostat=ios)
      listread = "phonons" 
      read(input_p_unit,phonons,err=200,iostat=ios)
      if(trim(calculation).eq.'resonant_raman')then
        listread = "resonant_raman" 
        read(input_p_unit,resonant_raman,err=200,iostat=ios)
      end if

    end if

    call check_on_input( )

    nat = atoms_nmbr
    nmodes = nat*3


    if(calculation.eq.'migdal_eliashberg') then

      ios=0

      call input_migdal_defaults()

      read(input_p_unit,input_migdal,err=300,iostat=ios)

      300  continue
      if(ios.ne.0) then 
        write(stdout,*) 'error in reading input_migdal',ios
        stop
      endif

      call check_on_input_migdal(stdout)
    endif


    if (nproc .gt.1)then
      write(stdout,"(/,1x,'Running in PARALLEL mode on ',i4,' processors',/)")nproc
    else
      write(stdout,*) ' Running in SERIAL mode'
    end if

    !-----------------------------------------	
    !          read U_mn(k) from wannier     |
    !-----------------------------------------
    write(stdout,*) 'Reading Wannier information'
    !  call read_U_from_wannier(real_lattice, recip_lattice, old_format_dat,prefix,stdout)

    if(old_wannier_format) then
      call read_U_from_wannier(real_lattice, recip_lattice, .false.,prefix,stdout)
      !call read_U_from_wannier(real_lattice, recip_lattice, old_format_dat,prefix,stdout)
    else
      call param_read_chkpt_and_eig(real_lattice, recip_lattice, prefix)

      call wannier_report(real_lattice, recip_lattice, num_bands, num_wann, num_kpts, &
        have_disentangled, omega_invariant, wannier_centres, wannier_spreads, stdout)
    endif


    !-------------------------------------------------
    !Then I read q-points from gmats the xq
    !-------------------------------------------------

    if (q_points_file.ne.' ')then
      write(stdout,*) 'Reading q points file'
      call read_q_points_file(q_points_file,nq,xq,celldm(1), mp_grid_q,real_lattice)     
    else
      call read_q_points_from_gmat(nq,xq,celldm,prefix,stdout)
    end if


    if(Fourier_interp_dyn) then
      do i=1,3
        do j=1,3
          at_local(i,j)=real_lattice(j,i)/bohr/celldm(1)
          bg_local(i,j)=recip_lattice(j,i)*celldm(1)*bohr/twopi
        enddo
      enddo
    endif



    if(calculation.eq.'phonon_frequency_grid'.or.calculation.eq.'phonon_frequency') then
      do i=1,3
        do j=1,3
          at_local(i,j)=real_lattice(j,i)/bohr/celldm(1)
          bg_local(i,j)=recip_lattice(j,i)*celldm(1)*bohr/twopi
        enddo
      enddo
    endif


    ! -----------------------------------------
    !   if the calculation is phonon frequency then
    !  (i) I read the irreducible q-points from dynq0
    !  (ii) I read the ph k_points grid from the input file
    !------------------------------------------

    if(calculation.eq.'phonon_frequency_grid'.and..not.use_alternative_dyn) then
      call read_ph_dynq0(celldm(1),real_lattice, ph_irr_dyn_prefix)
    elseif(use_alternative_dyn) then
      call read_ph_dynq0(celldm(1),real_lattice, prefix_alt_dyn)
    endif
    if(diff_start_file.ne." ")then
      write(stdout, '(3x,"Starting differential parameters read (eV):")')
      write(stdout, '(6x,a,f12.6)') 'nel_r = ',nel_r
      write(stdout, '(6x,a,f12.6)') 'efermi = ',efermi
      write(stdout, '(6x,a,f12.6)') 'homo = ',homo
      write(stdout, '(6x,a,f12.6)') 'lumo = ',lumo

      if((calculation.eq.'phonon_frequency').or.&
        (calculation.eq.'phonon_frequency_grid')  ) then 
        call read_ph_kpoint_grid(diff_start_param_iu, celldm(1),real_lattice,stdout)  

        write(stdout, '(6x,a,f12.6)') 'sigma_ph = ',sigma_ph
        write(stdout, '(6x,a,i3)') 'ngauss_ph = ',ngauss_ph
        write(stdout, '(6x,a,i9)') 'nk_int_ph = ',nk_int_ph
      endif
      write(stdout, '(/)') 
    endif

    !-----------------------------------------
    !   reading k=points for interpolation
    !-----------------------------------------


    if(interpolate_k.and.(calculation.ne.'phonon_frequency_grid') ) then

      write(stdout,*) 'Reading/calculating k and q-points for interpolation'

      if(calculation.eq.'migdal_eliashberg') then
        call read_k_and_q_points_to_interp(celldm(1),&
          real_lattice,nproc,pe_id,.true.,.false.)

      else
      !elseif(calculation.eq.'resonant_raman') then
        call read_weighted_k_and_q_points_to_interp(celldm(1),&
          real_lattice,nproc,pe_id,.true.,.true.)
          if (Nqlt.eq.0) Nqlt=nq_int
          
      !else
        !call read_k_and_q_points_to_interp(celldm(1),&
          !real_lattice,nproc,pe_id,.true.,.true.)
      endif

      write(stdout,*) 'Done'

    endif



    close(input_p_unit)

    IF((file_in.eq."input_tmp.in").and.ionode)then
      WRITE(stdout,'(5x,"Deleting temporary input file: ",a)')file_in
      INQUIRE( FILE=TRIM(file_in), EXIST=exst )
      IF( exst ) THEN
        OPEN( UNIT=input_p_unit, FILE=TRIM(file_in), STATUS='OLD',IOSTAT=ios )
        IF (ios==0) THEN
          CLOSE( UNIT=input_p_unit, STATUS='DELETE', IOSTAT=ios )
        ELSE
          WRITE(stdout,'(5x,"Remark: ",a," file could not be deleted")')file_in
        END IF
      ELSE
        WRITE(stdout,'(5x,"Remark: ",a," file not exist?!")')file_in
      END IF
    endif



  end subroutine init_and_read_input





  !
  ! Setting the namelist defaults for input
  !

  subroutine input_namelist_defaults(  )
    use w90_constants, only : dp
    use parallel_var,only: nproc

    implicit none

    !  internal
    Ef_from_input = .false.
    fdos_only =.false.
    nel_r = 0.d0
    bandres_lw = .false.
    holes=.false.
    wan_lt=1
    wan_start=1
    wan_end=0
    theta=300.0   !room T
    calculation='ph_linewidth'
    verbosity='high'
    ngauss=0
    sigma_min=0.02
    sigma_max=0.1
    dsigma=0.01
    interp_mode_file='matdyn.eig'
    interp_dyn_file='matdyn.dyn'
    fil_dyn_ext='Extr.dyn'
    read_dumped_gR=.false.
    dump_gR=.false.
    gR_filename='G_and_H.bin'

    nfreq=1
    freq_min=0
    freq_max=0
    nbroad=1
    broad_min=0
    broad_max=0
    freq_broad_units = "Ryd"

    laser_nfreq=100
    laser_freq_min=0.0    !units in meV
    laser_freq_max=100.0  !units in meV
    thr_compute_k=0 !3*sigma !*nsigma
    ngauss_ph=1
    sigma_ph=0.0
    efermi=100.d0
    diel_file='dynq1' !not in namelist anymore, always dynq1
    diel=.false.
    defects=.false. !obsolete variable
    fet=.false.
    write_bands=.false.
    write_modes=.false.
    write_dyn=.false.
    read_modes=.false.
    chosen_sigma=0.1
    laser_nbroad=1
    iseed=0
    acoustic_sum_rule=.false.
    use_alternative_dyn=.false.
    prefix_alt_dyn=' '
    alternative_mass=-100.d0
    Nqlt=0

    ascii_G_and_H=.false.

    nq1=0
    nq2=0
    nq3=0

    !-----------------------------------------	
    ! other defaults
    !-----------------------------------------	

    interpolate_k=.true.

    Fourier_interp_dyn=.true.


    !dr_raman
    modes_min=1
    factgamma=1.0
    gw_correction=.true.
    compute_gammak=.false.
    !raman_type='standard'
    raman_type='carbon'
    laser_freq_min = 2.40d0
    laser_freq_max = 2.40d0
    laser_nfreq = 1
    laser_broad_min = 0.1
    laser_broad_max = 0.1
    laser_nbroad = 1
    !dr_raman
    old_wannier_format=.false.
    q_points_file=' '

    diff_start_file=' '
    commutator_file=' '
    elphmat_dir='./' 

    forceq_dot5 = .false.
    use_ws_distance = .true.
    use_ws_distance_elph = .false.
    ordered_dyn_file = .true.
    out2json=.false.

    main_drive=.true.
    post_processing=.false.

    system2d = .false.

    atoms_nmbr = 1

    drr_pp_nproc = nproc

  end subroutine input_namelist_defaults


  !-----------------------------------------	
  !          read input from input_pe         |
  !-----------------------------------------
  ! beta, sigma_min, sigma_max, sigma_min_ryd, sigma_max_ryd, nsigma, conv_factor

  subroutine check_on_input(   )
    use w90_constants, only : dp, ryd2eV,ev2kelvin,ev2cm_1
    use espresso_dynamical_matrix, only : xq
    use kq_points,     only : nq
    use control_flags, only : adiabatic, non_adiabatic,do_commutator
    use io_var,only: stdout,  io_file_unit,error,err_str,exst,str_to_lower
    implicit none
    integer :: iw

    if (dump_gR) interpolate_k=.false.
    if (dump_gR) read_dumped_gR=.false.

    if(nq1.le.0.or.nq2.le.0.or.nq3.le.0) then
      write(stdout,*) 'Please provide valid nq1, nq2, nq3 in input !'
      stop
    else
      mp_grid_q(1)=nq1
      mp_grid_q(2)=nq2
      mp_grid_q(3)=nq3
      nq= mp_grid_q(1)*mp_grid_q(2)*mp_grid_q(3)
      allocate(xq(3,nq))
    endif

    non_adiabatic=.false.

    if(calculation.eq.'phonon_frequency_na'.or. &
      calculation.eq.'phonon_frequency_NA') then
      if(Fourier_interp_dyn) then
        write(stdout,*) calculation,' requires Fourier_interp_dyn=.false.'
        write(stdout,*) 'set to .false.'
        Fourier_interp_dyn=.false.
      endif
      calculation='phonon_frequency'
      non_adiabatic=.true.
      adiabatic=.true.
    elseif(calculation.eq.'phonon_frequency_na_only'.or. &
        calculation.eq.'phonon_frequency_NA_only') then
        if(Fourier_interp_dyn) then
          write(stdout,*) calculation,' requires Fourier_interp_dyn=.false.'
          write(stdout,*) 'set to .false.'
          Fourier_interp_dyn=.false.
        endif
        calculation='phonon_frequency'
        non_adiabatic=.true.
        adiabatic=.false.
      elseif(calculation.eq.'phonon_frequency') then
        if(Fourier_interp_dyn) then
          write(stdout,*) calculation,' requires Fourier_interp_dyn=.false.'
          write(stdout,*) 'set to .false.'
          Fourier_interp_dyn=.false.
        endif
        non_adiabatic=.false.
        adiabatic=.true.
      endif

      if(calculation.eq.'phonon_frequency_grid'.and.read_dumped_gR) then
        write(stdout,*)
        write(stdout,*) 'WARNING read_dumped_gR=.true. and calculation=phonon_frequency_grid'
        write(stdout,*) ' is not allowed. setting read_dumped_gR=.false.'
        write(stdout,*)
        read_dumped_gR=.false.
      endif



      if((calculation.eq.'phonon_frequency').or.(calculation.eq.'phonon_frequency_grid'))then

        diff_start_file=trim(diff_start_file)
        INQUIRE(FILE=diff_start_file, EXIST=exst)
        if(.not.exst) then
          call error('check_on_input','Provide a valid file for &
            "control>diff_start_file" instead of "'//diff_start_file//'" file.',1)
        endif

        if (sigma_ph.lt.1.d-6) then
          write(stdout,*) 'sigma_ph=',sigma_ph, 'is not allowed'
          sigma_ph = 1d-6
          write(stdout,*) 'setting it to a small value sigma_ph=',sigma_ph
          !call error('check_on_input',err_str,1)
        endif
        if( efermi.eq.100) then
          efermi=0.5*(homo+lumo)
        endif


        if(sigma_min.lt.1.d-6) then
          write(err_str,*) 'sigma_min=',sigma_min, 'is not allowed'
          call error('check_on_input',err_str,1)
        endif

      endif

      if(calculation.eq.'resonant_raman') then
        do_commutator = .true.

        if(nsigma.gt.1) then
          write(stdout,*) 'For resonant raman calculation nsigma is set to 1'
          nsigma=1
        endif


        gR_filename='G_and_H_and_chi.bin'
        if(ascii_g_and_H) gR_filename='G_and_H_and_chi.asc'
      else
        gR_filename='G_and_H.bin'
        if(ascii_g_and_H) gR_filename='G_and_H.asc'
      endif


      if(verbosity.ne.'low'.and.verbosity.ne.'high') then
        write(stdout,*) 'Verbosity must be high or low'
        write(stdout,*) 'setting it to low'
        verbosity='low'
      endif

      if(use_alternative_dyn.and.prefix_alt_dyn.eq.' ') then
        call error('input_param','Please specify prefix_alt_dyn',2)
      endif

      if(commutator_file.ne.' ') then 
        write(stdout,*) 'Commutator_file initialized: setting do_commutator = .true.'
        do_commutator = .true.
      end if


      if(do_commutator)then
        if(commutator_file.eq.' ') then
          call error('check_on_input','Provide a valid name for &
            commutator_file',2)
        else
          commutator_file=trim(commutator_file)
          INQUIRE(FILE=commutator_file, EXIST=exst)
          if(.not.exst) then
            call error('check_on_input','Provide a valid file for &
              "electron>commutator_file" instead of "'//commutator_file//'" file.',1)
          endif
        endif
      endif


      !-----------------------------------------	
      !          definition of some vars        |
      !-----------------------------------------

      theta=theta/ev2kelvin   !conversion between Kelvin and eV
      beta=1.d0/theta
      sigma_min_ryd=sigma_min
      sigma_max_ryd=sigma_max

      sigma_ph=sigma_ph*ryd2eV
      sigma_min=sigma_min*ryd2eV
      sigma_max=sigma_max*ryd2eV

      if(nsigma.gt.1) then
        dsigma=(sigma_max-sigma_min)/(nsigma-1)
        dsigma_ryd=(sigma_max_ryd-sigma_min_ryd)/(nsigma-1)
      else
        dsigma=0.d0
        dsigma_ryd=0.d0
      endif


      allocate(broad_lst(nbroad))
      if(nbroad .gt. 1)then
        do iw=1,size(broad_lst) 
          broad_lst(iw) = broad_min + (iw-1)*(broad_max-broad_min)/(nbroad-1)
        enddo
      else
        broad_lst(1) =  broad_min
      end if

      allocate(freq_lst(nfreq))
      if(nfreq .gt. 1)then
        do iw=1,size(freq_lst) 
          freq_lst(iw) = freq_min + (iw-1)*(freq_max-freq_min)/(nfreq-1)
        enddo
      else
        freq_lst(1) =  freq_min
      end if

      freq_broad_units = str_to_lower(trim(adjustl(freq_broad_units)))
      if ((freq_broad_units.ne."ryd") .and. (freq_broad_units.ne."ev").and. (freq_broad_units.ne."cm-1")) &
        call error('check_on_input',"wrong freq_broad_units: "//trim(freq_broad_units)//" not in ('ryd','ev','cm-1')",1)
      if (freq_broad_units.eq."ryd")then
        freq_lst=freq_lst*ryd2ev
        broad_lst=broad_lst*ryd2ev
      elseif (freq_broad_units.eq."cm-1")then
        freq_lst=freq_lst/ev2cm_1
        broad_lst=broad_lst/ev2cm_1
      endif


      if(non_adiabatic.and.abs(freq_lst(1)).lt.7.d-5) then
        call error('check_on_input' ,'the bare phonon frequency cannot be below 1 meV',1)
      endif

      if(non_adiabatic.and.abs(broad_lst(1)).lt.1.d-5) then
        write(err_str,*) 'Too small broad_lst(1)=',broad_lst(1)
        call error('check_on_input',err_str,1)
      endif


      thr_compute_k=thr_compute_k*ryd2eV

      conv_factor_lw=ryd2eV*1000.d0

      calculation=trim(adjustl(calculation))


    elphmat_dir=trim(adjustl(elphmat_dir))//'/' 

    end subroutine check_on_input

    !=========================================================== 
    !-----------------------------------------	
    !          read ph.x phonon q-points grid |
    !-----------------------------------------
    subroutine read_ph_kpoint_grid(inp_unit, celldm,real_lattice,stdout)
      use w90_constants, only : dp,twopi,bohr
      use kq_points,     only : nk_int_ph, xk_int_ph, wk_ph
      use utility,   only : utility_cart_to_frac
      use io_var, only : io_file_unit,str_to_lower,error,err_str

      implicit none
      integer                :: inp_unit, stdout,iounit, ios
      ! internal
      integer                         :: nk1_ph,nk2_ph,nk3_ph,i,j,k,n
      integer                         :: ish1, ish2, ish3
      CHARACTER(len=20)                  ::k_string
      CHARACTER(len=256)                  ::filename
      real(kind=dp)          :: xq_s(3)
      real(kind=dp)          :: celldm,real_lattice(3,3)
      real(kind=dp)          :: wktot
      read(inp_unit,*) k_string
      k_string = str_to_lower(trim(adjustl(k_string)))
      if(k_string.ne.'kpoints') then
        write(err_str,*) 'Need to define ph_grid! I read insted: ', k_string
        call error('read_ph_kpoint_grid',err_str,1)
      end if

      read(inp_unit,*) k_string
      k_string = str_to_lower(trim(adjustl(k_string)))


      if(k_string.eq.'automatic') then
        write(stdout,*) 'automatic generation of ph k-points'
        read(inp_unit,*) nk1_ph,nk2_ph,nk3_ph, ish1, ish2, ish3
        nk_int_ph=nk1_ph*nk2_ph*nk3_ph
        write(stdout,*) 'nk_int_ph=',nk_int_ph

        allocate(xk_int_ph(3,nk_int_ph))
        allocate(wk_ph(nk_int_ph))
        wk_ph=1.0_dp/nk_int_ph

        do i=0,nk1_ph-1
          do j=0,nk2_ph-1
            do k=0,nk3_ph-1
              !  this is nothing but consecutive ordering
              n = k + j*nk3_ph + i*nk2_ph*nk3_ph + 1
              !  xkg are the components of the complete grid in crystal axis
              xk_int_ph(1,n) = DBLE(i)/nk1_ph + DBLE(ish1)/nk1_ph/2.d0
              xk_int_ph(2,n) = DBLE(j)/nk2_ph + DBLE(ish2)/nk2_ph/2.d0
              xk_int_ph(3,n) = DBLE(k)/nk3_ph + DBLE(ish3)/nk3_ph/2.d0
            end do
          end do
        end do

      else if(trim(adjustl(k_string)).eq.'file-tpiba') then
        read(inp_unit,'(a)') filename
        filename = trim(adjustl(filename))
        write(stdout,*) 'Reading ph_grid from file: '
        write(stdout,*) filename

        iounit=io_file_unit()
        open(unit=iounit,file=filename,status='unknown',form='formatted',IOSTAT=ios)
        if(ios.ne.0) then
          write(6,*) 'ERROR reading ',filename
        end if 

        read(iounit,*) nk_int_ph

        allocate(xk_int_ph(3,nk_int_ph))
        allocate(wk_ph(nk_int_ph))
          wktot=0.d0
        do k=1,nk_int_ph
          read(iounit,*) (xk_int_ph(i,k),i=1,3),wk_ph(k)
          wktot=wktot+wk_ph(k)
        enddo
         do k=1,nk_int_ph
         wk_ph(k)=wk_ph(k)/wktot
         end do
        xk_int_ph=xk_int_ph*twopi/celldm/bohr
        do n=1,nk_int_ph
          call utility_cart_to_frac(xk_int_ph(:,n),xq_s,real_lattice)
          xk_int_ph(:,n)=xq_s(:)
        enddo

     elseif(trim(adjustl(k_string)).eq.'crystal'.or.trim(adjustl(k_string)).eq.'CRYSTAL') then
        write(stdout,*) 'reading k-points in reduced coordinates from input'
        read(inp_unit,*) nk_int_ph
        allocate(xk_int_ph(3,nk_int_ph))
        allocate(wk_ph(nk_int_ph))
        !wk_ph=1_dp/real(nk_int_ph,dp)
          wktot=0.d0
        do k=1,nk_int_ph
           read(inp_unit,*) (xk_int_ph(i,k),i=1,3), wk_ph(k)
           wktot=wktot+wk_ph(k)
        enddo
        wk_ph(:)=wk_ph(:)/wktot

     elseif(k_string.eq.'tpiba') then
        write(stdout, '(3x,"Reading starting diff k-points in cartesian coordinates")')
        read(inp_unit,*) nk_int_ph
        allocate(xk_int_ph(3,nk_int_ph))
        allocate(wk_ph(nk_int_ph))
          wktot=0.d0
        do k=1,nk_int_ph
           read(inp_unit,*) (xk_int_ph(i,k),i=1,3), wk_ph(k)
           wktot=wktot+wk_ph(k)
        enddo
        do k=1,nk_int_ph
          wk_ph(k)=wk_ph(k)/wktot
        end do
        xk_int_ph=xk_int_ph*twopi/celldm/bohr
        do n=1,nk_int_ph
           call utility_cart_to_frac(xk_int_ph(:,n),xq_s,real_lattice)
           xk_int_ph(:,n)=xq_s(:)
        enddo

      else
        call error('read_ph_kpoint_grid','read unkwown type: '// k_string,1)
      endif
    end subroutine read_ph_kpoint_grid




    subroutine read_q_points_file(q_points_file,nq,xq,celldm, mp_grid_q,real_lattice)
      use w90_constants, only : dp
      use io_var, only : io_file_unit
      use w90_constants, only : twopi,bohr
      use utility,   only : utility_cart_to_frac
      implicit none
      integer                       :: mp_grid_q(3)
      real(kind=dp)                 :: celldm,real_lattice(3,3)
      character(len=75)             :: q_points_file
      !
      logical                       :: lcryst
      real(kind=dp)                 :: xq_s(3)
      real(kind=dp), INTENT(OUT) :: xq(3,nq)
      integer                       :: iounit2,nq1,nq2,nq3,n,nq

      iounit2=io_file_unit()
      open(unit=iounit2,file=trim(adjustl(q_points_file)),status='unknown',action='read')
      rewind(iounit2)
      read(iounit2,*) nq1,nq2,nq3,lcryst,celldm

      mp_grid_q(1)=nq1
      mp_grid_q(2)=nq2
      mp_grid_q(3)=nq3
      nq= mp_grid_q(1)*mp_grid_q(2)*mp_grid_q(3)


      !allocate(xq(3,nq))

      do n=1,nq
        read(iounit2,*) xq(1,n),xq(2,n),xq(3,n)
      enddo

      close(iounit2)

      if(.not.lcryst) then   !convert in crystalline

        !        write(stdout,*) 'q_points read from file are (units 2pi/a_0)'
        !        do n=1,nq
        !           write(stdout,'(1i3,3f14.8)') n,(xq(i,n),i=1,3)
        !        enddo

        xq=xq*twopi/celldm/bohr

        do n=1,nq
          call utility_cart_to_frac(xq(:,n),xq_s,real_lattice)
          xq(:,n)=xq_s(:)
        enddo

      endif



    end subroutine read_Q_POINTS_file

  end Module input_param

