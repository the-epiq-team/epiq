!    EPIq v 1.2, Electron-phonon interaction over k and q points
!    Authors : Matteo Calandra, Gianni Profeta, Francesco Mauri, Giulio Volpato,
!    Nathalie Vast, Jelena Sjakste, Francesco Macheda, Giovanni Marini, Guglielmo Marchese

!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.

!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.

!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.


program elphmat_modify
  use w90_constants, only : pi,twopi,bohr,dp,ryd2ev
  implicit none
  character(len=256) :: filelphmat, fileout
  integer :: nbnd_min,nbnd_max,nbnd_r,nmodes, nk_r, nat, ntyp
  integer :: ibrav, nbnd, num_kpts
  integer :: nsym,nq,imq
  integer :: k,nu,mu,i,j,ios,idum,jdum,kdum
  integer :: invs(48),isq(48),s(3,3,48)
  integer, allocatable ::  ityp(:),irt(:,:)
  real(dp) :: nel_r
  real(dp) :: xq_r(3),celldm(6),at(3,3),bg(3,3)
  REAL(DP), allocatable :: rtauloc(:,:,:)
  real(DP) :: sxq (3, 48)	
                                        
  real(dp), allocatable :: amass(:), tau(:,:)
  real(dp), allocatable :: eig(:,:), w2(:),xk_r(:,:)
  complex(dp), allocatable :: zz(:,:), dyn(:,:)
  complex(dp), allocatable :: g_matrix(:,:,:,:)
  CHARACTER(LEN=3),allocatable      :: atm(:)
  real(dp) :: sumband
!
!  new vriables to be inserted
!
  logical :: noncolin, lborn
  integer :: nspin

  noncolin=.false.
  lborn=.false.
  nspin=1

  read(5,*) filelphmat

  fileout=trim(adjustl(filelphmat))//'.mod'
  
  open(unit=10,file=trim(adjustl(filelphmat)),status='unknown',form='unformatted') 
  rewind(10)

  READ (10) (xq_r(j),j=1,3)

  READ (10) nel_r
     
  READ (10) nbnd_min,nbnd_max,nbnd_r !nbnd_r = total number of bands in pw 
! write(6,*) nbnd_min,nbnd_max,nbnd_r 
  READ (10) nmodes, nk_r, nat, ntyp
! write(6,*) nmodes, nk_r, nat, ntyp  
  READ (10) ibrav,(celldm(j),j=1,6)
  
  allocate(atm(ntyp),amass(ntyp),ityp(nat),tau(3,nat))
     
  READ (10)  (atm(j),j=1,ntyp),(amass(j),j=1,ntyp), &
       (ityp(j),j=1,nat),((tau(j,i),j=1,3),i=1,nat)


  nbnd=nbnd_max-nbnd_min+1 !nbnd_min,nbnd_max are read from PH
  num_kpts=nk_r

  ALLOCATE(eig(nbnd,num_kpts))
  ALLOCATE(w2(nmodes),xk_r(3,num_kpts))
  ALLOCATE(zz(nmodes,nmodes))
  ALLOCATE(dyn(nmodes,nmodes))

  ALLOCATE(g_matrix(nbnd,nbnd,nmodes,num_kpts))

  READ (10) (w2 (nu) , nu = 1, nmodes)
  READ (10) ((zz(i,j),i=1,nmodes),j=1,nmodes)   !eigenvectors in the QE basis
  READ (10) ((dyn(i,j),i=1,nmodes),j=1,nmodes)
  
  do k=1,num_kpts
     READ (10) (xk_r(i,k),i=1,3)
     READ (10) (eig(i,k),i=1,nbnd)
     
     do nu=1,nmodes
        READ (10) &
             ((g_matrix(j, i, nu, k),j=1,nbnd),i=1,nbnd)
        
     enddo
  enddo

  do j=1,3
    read(10) (at(i,j),i=1,3)
  enddo
  do j=1,3
     read(10) (bg(i,j),i=1,3)
  enddo
  read(10) nsym,nq,imq

  
  allocate(rtauloc(3,nsym,nat))
  allocate(irt(nsym,nat))
  do i=1,nsym
     read(10) idum,invs(idum),isq(idum)
     do j=1,3
        do k=1,3
          read(10) kdum,jdum, s(kdum,jdum,idum)
        enddo
     enddo
     do j=1,nat
        read(10) jdum, irt(i,jdum) 
     enddo
     do j=1,3
        do k=1,nat
           read(10) jdum,idum, rtauloc(jdum,idum,k)  
        enddo
     enddo
     do j=1,3
        read(10) jdum, sxq(jdum,i)
     enddo
  enddo
     

  close(10)  

  open(unit=20,file=trim(adjustl(fileout)),status='unknown',err=111, &
                    iostat = ios, form='unformatted') 
  rewind(20)
111 if(ios.ne.0) stop

  WRITE (20) (xq_r(j),j=1,3)
  WRITE (20) noncolin, nspin, lborn
  WRITE (20) nel_r
     
  WRITE (20) nbnd_min,nbnd_max,nbnd_r !nbnd_r = total number of bands in pw 
! write(6,*) nbnd_min,nbnd_max,nbnd_r 
  WRITE (20) nmodes, nk_r, nat, ntyp
! write(6,*) nmodes, nk_r, nat, ntyp  
  WRITE (20) ibrav,(celldm(j),j=1,6)
  
  WRITE (20)  (atm(j),j=1,ntyp),(amass(j),j=1,ntyp), &
       (ityp(j),j=1,nat),((tau(j,i),j=1,3),i=1,nat)


  nbnd=nbnd_max-nbnd_min+1 !nbnd_min,nbnd_max are read from PH
  num_kpts=nk_r

  WRITE (20) (w2 (nu) , nu = 1, nmodes)
  WRITE (20) ((zz(i,j),i=1,nmodes),j=1,nmodes)   !eigenvectors in the QE basis
  WRITE (20) ((dyn(i,j),i=1,nmodes),j=1,nmodes)
  
  do k=1,num_kpts
     WRITE (20) (xk_r(i,k),i=1,3)
     WRITE (20) (eig(i,k),i=1,nbnd)
     
     do nu=1,nmodes
        WRITE (20) &
             ((g_matrix(j, i, nu, k),j=1,nbnd),i=1,nbnd)
        
     enddo
  enddo

  do j=1,3
     write(20) (at(i,j),i=1,3)
  enddo
  do j=1,3
     write(20) (bg(i,j),i=1,3)
  enddo
  write(20) nsym,nq,imq
  do i=1,nsym
     write(20) i,invs(i),isq(i)
     do j=1,3
        do k=1,3
           write(20) k,j, s(k,j,i)
        enddo
     enddo
     do j=1,nat
        write(20) j, irt(i,j) 
     enddo
     do j=1,3
        do k=1,nat
           write(20) j,i, rtauloc(j,i,k)  
        enddo
     enddo
     do j=1,3
        write(20) j, sxq(j,i)
     enddo
  enddo
     
  close(20)

  DEALLOCATE(eig)
  DEALLOCATE(w2,xk_r)
  DEALLOCATE(zz)
  DEALLOCATE(dyn)

  DEALLOCATE(g_matrix)
  deallocate(rtauloc)
  deallocate(irt)


end program elphmat_modify
