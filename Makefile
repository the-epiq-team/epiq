ifndef ROOTDIR
mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
ROOTDIR := $(patsubst %/,%,$(dir $(mkfile_path)))
export
endif


help:
	@echo "This is the 'help' message for using this Makefile:"
	@echo "   - 'make all': compile all exectutables"
	@echo "   - 'make epiq': compile only the main drive epiq.x"
	@echo "   - 'make <system>': run tutorial of that system (mgb2,mos2,graphene) "


all:
	(cd $(ROOTDIR)/src && $(MAKE) all || exit 1 )

epiq:
	(cd $(ROOTDIR)/src && $(MAKE) epiq || exit 1)

clean:
	(cd $(ROOTDIR)/src && $(MAKE) clean || exit 1)
	
test_install:
	(cd $(ROOTDIR)/tutorials/graphene/automatic_run && $(MAKE) test_raman  || exit 1)
	(cd $(ROOTDIR)/tutorials/graphene/automatic_run && $(MAKE) test_ph_freq  || exit 1)
	(cd $(ROOTDIR)/tutorials/graphene/automatic_run && $(MAKE) test_na_ph_freq  || exit 1)


test_install_docker:
	(cd $(ROOTDIR)/tutorials/graphene/automatic_run && $(MAKE) test_raman_docker  || exit 1)
	(cd $(ROOTDIR)/tutorials/graphene/automatic_run && $(MAKE) test_ph_freq_docker  || exit 1)
	(cd $(ROOTDIR)/tutorials/graphene/automatic_run && $(MAKE) test_na_ph_freq_docker  || exit 1)
	(cd $(ROOTDIR)/tutorials/graphene/automatic_run && $(MAKE) test_linewidth_docker  || exit 1)

test_raman:
	(cd $(ROOTDIR)/tutorials/graphene/automatic_run && $(MAKE) test_raman || exit 1)
